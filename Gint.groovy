/*
 * Copyright (c) 2021 Appfire Technologies, LLC.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Bob Swift Atlassian Apps EULA"
 * (https://bobswift.atlassian.net/wiki/x/WoDXBQ) as well as under the provisions of
 * the "Standard EULA" from the "Atlassian Marketplace Terms of Use" as a "Marketplace Product"
 * (http://www.atlassian.com/licensing/marketplace/termsofuse).
 *
 * See the LICENSE file for more details.
 */
 
/**
 * <p> Gint and GintTargets combine to provide all the facilities to easily create and run integration or other tests.
 * A test is a Groovy script with testcases defined via Groovy or imported from CSV or read from a SQL database.
 * The test is run by running the test script using GANT (GANT adds targets and target dependency management to Groovy scripts).
 * GINT creates a target for each testcase. GANT's powerful target dependency management support is a critical
 * component of the GINT framework ensuring testcases are ordered correctly and run only once.
 * GINT provides the ability to run one or more individual testcases, groups of testcases, all testcases, or
 * all failed testcases. Results are logged and summarized with optional JUnit style XML reports.
 * Command line options and property file support allows for parameterized tests.
 * </p>
 * <p>Built in support includes the ability to determine the success or failure of a testcase based on various factors:</p>
 * <ul>
 * <li> result of command or inline code
 * <li> data found in output of command (list of strings or regex patterns)
 * <li> data that must NOT be found in output of command (list of strings or regex patterns)
 * <li> output file that needs to be produced by the command or inline code
 * <li> output data that needs to be found in the file produced by the command or inline code (list of strings or regex patterns)
 * <li> output data that must NOT be found in the file produced by the command or inline code (list of strings or regex patterns)
 * <li> file differences between current and expected output
 * <li> user defined closure with access to all testcase data
 * </ul>
 *
 * <p> Specific support available for:</p>
 * <ul>
 * <li> Shell commands (bat, bash, ...)
 * <li> Groovy commands
 * <li> GANT commands
 * <li> JAVA commands
 * <li> Atlassian CLIs (Confluence, JIRA, etc...) - see https://bobswift.atlassian.net/browse/ACLI
 * <li> Confluence macros
 * </ul>
 *
 * <p> Testcase definitions can be define by: </p>
 * <ul>
 * <li> Groovy/GANT script
 * <li> CSV file
 * <li> SQL query
 * </ul>
 *
 * <p> Example test file:
 * <pre>
 *    includeTool << org.swift.gint.Gint           // test infrastructure
 *
 *    gint.initialize(this) // initialize GINT - required
 *
 *    addSetup(...)         // add setUp testcase(s) - optional
 *    addTearDown(...)      // add tearDown testcase(s) - optional
 *    add(...)              // add testcase(s) - optional, but you should test something!
 *
 *    gint.finalizeTest()   // defines targets for all testcases, complete preparations for running - required
 * </pre>
 *
 * <p> Example runs:
 * <pre>
 *     gant -f test.gant
 *     gant -f test.gant testcaseA testcaseB testGroup1
 *     gant -f test.gant -Dclean -DsomeParameter=xxxx
 *     gant -f test.gant help
 * </pre>
 *
 * <p> GINT uses GANT's target mechanisms for running:
 * <ul>
 * <li> default target does the right thing - runs all testcases or last failed testcases
 * <li> individual targets run a single testcase or group of testcases (with dependencies, etc...)
 * <li> note that target names are case sensitive!
 * </ul>
 *
 * <p> Example testcase definitions - a testcase is a map with information that defines what will be done
 * <pre>
 * Simplest case:  [name: 'simple', inline: { true } ]                  - successful
 * Command case:   [name: 'dir', cmd: 'dir']                            - successful on Windows
 * Fail case:      [name: 'fail', inline: { false }, expected: false ]  - successful
 * Asserts:        [name: 'assert', inline: { assert 1 == 1 } ]         - successful
 * </pre>
 *
 * <p> Common parameters - see online information for all
 * <ul>
 * <li> -Dclean - ignore previous run state, run tearDown and setUp
 * <li> -DstopOnFail - stop running more testcases once a testcase fails
 * <li> -Dlevel=n (number) - run all testcases with level <= this number (default 1)
 * <li> -DmaxParallel=n (positive number) - maximum number of testcase that can run in parallel (default 8)
 * <li> -Dverbose - log additional information
 * <li> -DxmlReport or -DxmlReport=filename - create JUnit style output file
 * <li> -DmailReport or -DmailReport=xxxxx@gmail.com (requires mailConfig specified in properties)
 * <li> -DimReport or -DimReport=xxxxx@gmail.com (requires imConfig specified in properties)
 * </ul>
 *
 * <p> Properties for controlling GINT behavior (in order of precedence)
 * <ul>
 * <li> set directly in test script (prior to initialization)
 * <li> specified as command parameters
 * <li> set in a user specific property file (in user home directory)
 * <li> set in a property file in the current directory of the run
 * <li> set in a property file located in the project's resource directory
 * <li> set dynamically during test run using GINT methods (only selected values)
 * </ul>
 *
 * <p> Properties for use by the test - set in property files loaded as <b>profiles</b> during initialization, see online information
 *
 * <p> Testcase types and related design. All testcases go into testcasesRun and testcasesStarted as appropriate.
 * Each testcase goes through determineOutcome to determine its results even if the results are going to be ignored.
 * This means that all appropriate closures run (like resultClosure and successClosure) dependent on success of the
 * testcase. Result summary message is produced for testcases with ignoreFailure != true.
 * <ul>
 * <li> Normal testcase
 * <ul>
 * <li> default stopOnFail is global stopOnFail setting
 * <li> default ignoreFailure is false
 * <li> counts in normal counts section
 * </ul>
 * <li> SetUp testcase
 * <ul>
 * <li> default stopOnFail is true
 * <li> default ignoreFailure is false
 * <li> counts in setUp counts section, section normally only shown if there are failures
 * <li> marked as isSetUp = true on testcase
 * </ul>
 * <li> TearDown testcase
 * <ul>
 * <li> default stopOnFail is false
 * <li> default ignoreFailure is true
 * <li> counts in tearDown counts section, section normally only shown if verbose requested
 * <li> marked as isTearDown = true on testcase
 * </ul>
 * </ul>
 *
 * <p> Other notes:
 * <ul>
 * <li> all normal testcases are automatically depend on setUp testcases
 * <li> testcases can have their own dependencies
 * <li> testcases are generally run in order (with parallelism) except for dependencies or order settings
 * <li> testcases can have individual tearDown testcases to enable running a testcase over and over again with out setUp/tearDown targets
 *   <ul>
 *   <li> however, if full tearDown is done, individual tearDowns are not run
 *   </ul>
 * <li> the default target does the right thing depending on state:
 *   <ul>
 *   <li> runs all testcases including setUp and tearDown if there were no previous failing testcases
 *   <li> runs the last failed testcases (only)
 *   <li> use -Dclean to run full test no matter what
 *   </ul>
 * <li> individual testcases can be run
 * <li> runTestcases, setUp, tearDown targets can also be run specifically
 * </ul>
 */

package org.swift.tools

import groovy.sql.Sql
import groovy.transform.Synchronized
import groovy.xml.MarkupBuilder

import java.sql.SQLException
import java.util.regex.Pattern

import org.codehaus.gant.GantBinding
import org.codehaus.gant.GantState

/**
 * Class to provide common build script helper functions. Only for use from a Gant script via:
 * includeTool << com.vision.gant.tools.Helper
 * This has access to any gant script variable or function via: binding.getVariable('message')
 * While generally parameters are used, some pre-defined variables are used to make calls
 * simpler and easier to understand
 * @param mainDirectory
 * @param message - message logging function
 */
class Gint {

    // Constants
    public final gintVersion = '2.7.0-SNAPSHOT' // Build will set this to the correct level!

    public final int FAIL_TIMEOUT   = -96  // testcase was abandoned due to a timeout
    public final int FAIL_ERROR     = -97  // return code for a unknown, non-assert Error, example - a JAVA error
    public final int FAIL_ASSERTION = -98  // return code for a failed assertion
    public final int FAIL_EXCEPTION = -99  // return code for an exception raised

    protected final PROPERTY_FILENAME = 'gint.properties'
    protected final CURRENT_DIRECTORY = '.'
    protected final SOURCE_DIRECTORY = 'src/itest'
    protected final TARGET_DIRECTORY = 'target'
    protected final COMPARE_DIRECTORY = 'compare'
    protected final OUTPUT_DIRECTORY = 'output' // relative (default to target directory)
    protected final RESOURCE_DIRECTORY = 'resources'  // relative (default to source directory)
    protected final TEST_REPORTS_DIRECTORY = 'test-reports'  // relative (default to target directory)
    protected final PREFIX = 'z' // prefix for construct name
    protected final DISPLAY_LIMIT_SUMMARY = 300 // limit some potentially long logging requests for summaries
    protected final DISPLAY_LIMIT = 3000 // limit some potentially long logging requests, use verbose for more
    protected final XML_ENCODING = 'UTF-8' // XML report encoding
    protected final INCLUDE_ALL = 'all' // include level all
    protected final DEFAULT_REPORT_PROPERTY_EXCLUDES = 'password' // filter out properties in reports that contain password in the name
    protected final RESERVED_NAME_LIST = [
        'match', // this is the only one that appears to cause problems
    ]

    // Variables
    protected final GantBinding binding = null
    protected final Map initParameters = null
    protected Closure message
    protected Closure defaultCmdGenerator = null
    protected Closure cmdLogClosure = { entry -> entry + '\n' }
    protected Closure addClosure = null
    protected Closure sortClosure = { testcase -> (testcase.order instanceof Integer) ? testcase.order : Integer.MAX_VALUE - 10 } // default is sort by order
    protected Closure reportFilter = null
    protected Parallel parallel = null
    protected Helper helper = null
    protected GintIm gintIm = null
    protected GintCmdGenerator gintCmdGenerator = null

    protected directories = [:]
    protected hasInputDirectoryBeenCreated = false
    protected hasOutputDirectoryBeenCreated = false
    protected hasTestReportsDirectoryBeenCreated = false
    protected hasCompareDirectoryBeenCreated = false
    protected boolean hasTearDownRun = false
    protected boolean testFailed = false // test fails if any non-ignored testcase fails
    protected testFailedMessageList = [] // list of failure messages
    //protected boolean lastResult = true // remember result of last (non-ignored) testcase
    protected boolean stopNow = false // determine if test should stop
    protected int nameCounter = 1 // counter auto generating names for testcases with no name specified
    protected String lastTestcaseName = null // last name processed in addTarget
    protected skippedLevels = []as Set // track the various levels that are skipped
    protected activeThreadCount = 0  // track number of testcase threads going
    protected startTime = null
    protected diffCli = "diff -r -y --suppress-common-lines" // default diff, -y is side by side, -r is recursive through subdirectories

    // parameters are set during initialization from properties and command line parameters
    protected stopOnFail = false // true to stop running testcases once a testcase failure is detected
    protected noTearDown = false // always tearDown unless explicitly requested not
    protected clean = false  // true to ignore previous results for default start, don't remember status
    protected verbose = false  // true to show more output from successful testcases, stack traces
    protected simulate = false
    protected ignoreDependsResult = false // true to not skip testcase if one depends testcase failed or skipped
    protected ignoreDepends = false // true to not start depends testcases or targets when testcase starts
    protected int maxParallel = 8 // maximum number of concurrent testcases
    protected int maxThreads = 0 // allow it to default to maxParallel if not set
    protected int timeout = 0 // timeout in milliseconds for running testcase, 0 or negative is no timeout
    protected int autoRetry = 0 // global retry count applied for any testcase that doesn't have retry set, a way to globally apply a retry on all testcases
    protected prefix = PREFIX // prefix to enable uniqueness for concurrent runs
    protected label = '' // label for a test run, usually a version indicator. Used in setting up input/output directory structure
    protected compareLabel = '' // label for a completed test run to compare with
    protected level = 1 // run level, run all testcases <= this level, or if set to false
    protected includeLevels = [true]
    protected excludeLevels = [false]
    protected xmlReport = null // xmlReport file if requested, use -DxmlReport to use defaults
    protected dbReport = null
    protected dbReportConnection = null // dbReport parameter must represent a valid db profile
    protected cmdLog = null // log file
    protected cleanOutputDirectory = false  // true to clean the output directory before starting
    protected includedPropertiesLoaded = false // can only be loaded once

    // potential parameters
    protected encoding = null // file encoding for IO

    protected synchronizeClosures = false // run all testcase closures through a synchronized method

    // settings
    protected testName = null // set in initialize
    protected constructName = null // defaulted during initialization for space name (Confluence), project name (JIRA, Bamboo), etc...
    protected showFailDetail = true // set to false to suppress fail detail reporting in log output and xmlReport
    protected propertyFile = null // set in initialize
    protected properties = null // set in initialize
    protected patternFlag = Pattern.LITERAL // for string comparison matching - match literally by default

    // testcase lists
    protected setUpTestcases = []    // testcases marked as setUp
    protected tearDownTestcases = [] // testcases marked as tearDown
    protected testcases = []         // testcases that are not setUp or tearDown
    protected allTestcases = null    // all testcases (sum of testcases, tearDown and setUp testcases)
    protected testcasesRun = []      // testcases that have been run and ended
    protected testcasesStarted = []  // testcases that are currently started
    protected testcasesSkipped = []as Set // testcases that had to be skipped due to excluded levels or dependency
    // use a set to avoid duplicate counting of tear down tests skipped twice
    protected groupListMap = [:]     // map group name to list of associated testcases
    protected neededByMap = [:]      // map testcases (that have neededBy specified) to true or list of names

    /**
     *  Constructor for the "includeTool <<"  and "includeTool **" usage.
     *  @param binding The <code>GantBinding</code> to bind to.
     *  @param parameters The <code>Map</code> of initialization parameters.
     */
    Gint(final GantBinding binding, final Map parameters = null) {
        this.binding = binding
        this.initParameters = parameters

        def message = parameters?.message ?: binding?.variables?.message  // reuse existing message if available
        this.message = (message instanceof Closure) ? message : Helper.getStandardMessageClosure()

        def helper = parameters?.helper ?: binding?.variables?.helper   // reuse existing helper if available
        this.helper = (helper instanceof Helper) ? helper : new Helper(binding, parameters)

        this.parallel = parameters?.parallel instanceof Parallel ? parameters.parallel : new Parallel(binding, parameters)
        this.parallel.setLog(false)  // gint controls its own logging

        if (!binding.getVariables().containsKey('gint')) { // gint not already defined
            new GintTargets(binding, this)  // targets get associated to this instance of gint
        }
    }

    // Note: Even though Groovy allows us to NOT provide many getters/setters, we provide them explicitly for those
    // that we think should be exposed publicly.

    /**
     * Get IM support class if im support is needed
     * @return GintIm
     */
    public GintIm getGintIm() {
        if (this.gintIm == null) {
            this.gintIm = new GintIm(binding, this)
        }
        return this.gintIm
    }

    /**
     * Get cmd generator support class
     * @return GintCmdGenerator
     */
    public GintCmdGenerator getGintCmdGenerator() {
        if (this.gintCmdGenerator == null) {
            this.gintCmdGenerator = new GintCmdGenerator(binding, this)
        }
        return this.gintCmdGenerator
    }

    public String getGintVersion() {
        return gintVersion
    }

    /**
     * Get encoding for reading/writing files.
     * @return encoding from testcase has a specific one specified, otherwise use default setting. Could be null!
     */
    public String getEncoding(final Map testcase = null) {
        return testcase?.encoding ?: this.encoding
    }
    public void setEncoding(final String encoding) {
        this.encoding = encoding
    }

    public String getTestName() {
        return this.testName
    }
    public void setTestName(final name) {
        this.testName = name
    }
    protected getNextName() {
        return "${getPrefix()}${this.nameCounter++}"
    }
    /**
     * Many tests need to name a space, or project, or something. Often prefixing the testcase name
     * can prevent automation from conflicting with other usages.
     */
    public String getConstructName() {
        if (this.constructName == null) {
            return getPrefix() + getTestName()
        }
        return this.constructName
    }
    public void setConstructName(final name) {
        this.constructName = name
    }
    public String getPrefix() {
        return this.prefix
    }
    public void setPrefix(final String prefix) {
        this.prefix = prefix
    }
    public boolean getNoTearDown() {
        return this.noTearDown
    }
    public void setNoTearDown(final boolean noTearDown) {
        this.noTearDown = noTearDown
    }
    /**
     * Set noTearDown based on clean setting - do not run tearDown unless clean is explicitly requested
     */
    protected void setNoTearDown() {
        def clean = getClean()
        this.noTearDown = (clean != null ? !clean : true)
    }
    public boolean getShowFailDetail() {
        return this.showFailDetail
    }
    public void setShowFailDetail(final boolean showFailDetail) {
        this.showFailDetail = showFailDetail
    }
    public boolean getStopOnFail() {
        return this.stopOnFail
    }
    public void setStopOnFail(final boolean stopOnFail = true) {
        this.stopOnFail = stopOnFail
    }
    public boolean getClean() {
        return this.clean
    }
    public void setClean(final boolean clean = true) {
        this.clean = clean
    }
    public boolean getSimulate() {
        return this.simulate
    }
    public void setSimulate(final boolean simulate = true) {
        this.simulate = simulate
    }
    public boolean getIgnoreDependsResult() {
        return this.ignoreDependsResult
    }
    public void setIgnoreDependsResult(final boolean value = true) {
        this.ignoreDependsResult = value
    }
    public boolean getIgnoreDepends() {
        return this.ignoreDepends
    }
    public void setIgnoreDepends(final boolean value = true) {
        this.ignoreDepends = value
        if (this.ignoreDepends) {
            setIgnoreDependsResult(true)  // if depends testcase is not started, it will cause skipping unless this is also set
        }
    }

    /**
     * GINT specific verbose setting, although default setting based on GANT -v setting.
     * Choosing GANT -v setting will result in more underlying messages to be logged.
     * @return true to indicate that more verbose message should be logged
     */
    public boolean getVerbose() {
        return this.verbose
    }
    public void setVerbose(final boolean verbose) {
        this.verbose = verbose
    }
    public int getMaxParallel() {
        return this.maxParallel
    }
    public void setMaxParallel(final int maxParallel) {
        this.maxParallel = (maxParallel > 0) ? maxParallel : 1  // must be positive
    }
    public int getMaxThreads() {
        return (this.maxThreads > 0) ? this.maxThreads : getMaxParallel()  // use maxParallel as default when 0
    }
    public void setMaxThreads(final int maxThreads) {
        if (maxThreads > 0) { // < 1 means use maxParallel as default
            this.maxThreads = maxThreads  // must be positive
        }
    }
    public int getAutoRetry() {
        return this.autoRetry
    }
    public void setAutoRetry(final int autoRetry) {
        this.autoRetry = autoRetry
    }
    public int getTimeout() {
        return this.timeout
    }
    public void setTimeout(final int timeout) {
        this.timeout = timeout
    }
    public int getLevel() {
        return this.level
    }
    public void setLevel(final int level) {
        this.level = level
    }
    /**
     * get include level,
     * @return list of things to include or NULL means include everything
     */
    public List getIncludeLevels() {
        return this.includeLevels
    }
    public void setIncludeLevels(final List includeLevels) {
        this.includeLevels = includeLevels
    }
    public void setIncludeLevels(final String includeLevels) {
        this.includeLevels = (includeLevels.equalsIgnoreCase(INCLUDE_ALL)) ? this.includeLevels = null : helper.separatedStringToList(includeLevels)
    }
    public void addIncludeLevels(final List newLevels) {
        if (this.includeLevels != null) {
            this.includeLevels += newLevels
        }
    }
    public void addIncludeLevels(final String newLevels) {
        addIncludeLevels(helper.separatedStringToList(newLevels))
    }
    public List getExcludeLevels() {
        return this.excludeLevels
    }
    public void setExcludeLevels(final List excludeLevels) {
        this.excludeLevels = excludeLevels
    }
    public void setExcludeLevels(final String excludeLevels) {
        this.excludeLevels = helper.separatedStringToList(excludeLevels)
    }
    public void addExcludeLevels(final List newLevels) {
        if (this.excludeLevels != null) {
            this.excludeLevels += newLevels
        } else {
            this.excludeLevels = newLevels
        }
    }
    public void addExcludeLevels(final String newLevels) {
        addExcludeLevels(helper.separatedStringToList(newLevels))
    }
    public String getLabel() {
        return this.label
    }
    public void setLabel(final String label) {
        this.label = label
    }
    public String getCompareLabel() {
        return this.compareLabel
    }
    public void setCompareLabel(final String compareLabel) {
        this.compareLabel = compareLabel
    }

    public boolean getSynchronizeClosures() {
        return this.synchronizeClosures
    }
    public void setSynchronizeClosures(final boolean synchronizeClosures = true) {
        this.synchronizeClosures = synchronizeClosures
    }

    public Map<String, String> getDirectories() {
        return this.directories
    }
    public String getInputDirectory() {
        if (!hasInputDirectoryBeenCreated) {
            binding.ant.mkdir(dir: this.directories.input)
            this.hasInputDirectoryBeenCreated = true
        }
        return this.directories.input
    }
    public String setInputDirectory(final dir) {
        this.directories.input = dir
    }
    public String getOutputDirectory() {
        if (!hasOutputDirectoryBeenCreated) {
            if (getCleanOutputDirectory()) {
                (new File(this.directories.output)).deleteDir() // clean out everything
            }
            binding.ant.mkdir(dir: this.directories.output)  // ignore if already there
            this.hasOutputDirectoryBeenCreated = true
        }
        return this.directories.output
    }
    public String setOutputDirectory(final dir) {
        this.directories.output = dir
    }
    public String getResourceDirectory() {
        return this.directories.resource
    }
    public String setResourceDirectory(final dir) {
        this.directories.resource = dir
    }
    public String getSourceDirectory() {
        return this.directories.source
    }
    public String setSourceDirectory(final dir) {
        this.directories.source = dir
    }
    public String getTargetDirectory() {
        return this.directories.target
    }
    public String setTargetDirectory(final dir) {
        this.directories.target = dir
    }
    protected String getImageDirectory() { // internal only at least for now
        return getOutputDirectory() // same as output for now
    }
    public String getTestReportsDirectory() {
        if (!hasTestReportsDirectoryBeenCreated) {
            binding.ant.mkdir(dir: this.directories.testReports)
            this.hasTestReportsDirectoryBeenCreated = true
        }
        return this.directories.testReports
    }
    public String setTestReportsDirectory(final dir) {
        return this.directories.testReports = dir
    }
    public String getCompareDirectory() {
        if (!hasCompareDirectoryBeenCreated) {
            binding.ant.mkdir(dir: this.directories.compare)
            this.hasCompareDirectoryBeenCreated = true
        }
        return this.directories.compare
    }
    public void setCompareDirectory(final dir) {
        this.directories.compare = dir
    }
    protected String getCompareImageDirectory() { // internal only
        return this.directories.compare  // same as compare for now
    }

    public boolean getCleanOutputDirectory() {
        return this.cleanOutputDirectory
    }
    public void setCleanOutputDirectory(final boolean cleanOutputDirectory = true) {
        this.cleanOutputDirectory = cleanOutputDirectory
    }

    /**
     * Needs to return 1 line of output for each line that is different!
     */
    public String getDiffCli() {
        return this.diffCli
    }
    public void setDiffCli(final cmd) {
        this.diffCli = cmd
    }

    /**
     * Get test failed status
     * @return true if this test has been determined to be failed
     */
    public boolean getTestFailed() {
        return this.testFailed
    }
    public void setTestFailed(final boolean value = true) {
        this.testFailed = value
    }

    /**
     * Used for noting unusual or severe errors in the test log at the end of the results. Normally fails the test.
     * This includes assert, exception, and error failures which are considered unusual or unexpected.
     * However, because of retry, assert and similar error, use markTestFailed = false to avoid failing entire test while still noting the situation
     * @param string
     * @param markTestFailed - fail entire test
     */
    public void addTestFailedMessage(final string, markTestFailed = true) {
        setTestFailed(markTestFailed)
        message 'error', string
        this.testFailedMessageList << string
    }

    /**
     * Get stop now setting.
     * @return true if it has been determined that no more testcases should be started
     */
    protected boolean getStopNow() {
        return this.stopNow
    }
    protected void setStopNow(final boolean value = true) {
        this.stopNow = value
    }

    public List<Map> getSetUpTestcases() {
        return this.setUpTestcases
    }
    public List<Map> getTearDownTestcases() {
        return this.tearDownTestcases
    }
    /**
     * Testcases that are not setUp or tearDown testcases
     * @return
     */
    public List<Map> getTestcases() {
        return this.testcases
    }

    /**
     * Testcases that have been run and ended
     * @return list of testcases that have run (completed)
     */
    public List<Map> getTestcasesRun() { // testcases that have been run
        return this.testcasesRun
    }

    /**
     * Testcases that have been run and ended
     * @return list of testcases that have run (completed)
     */
    public Collection<Map> getTestcasesSkipped() { // testcases that have been run
        return this.testcasesSkipped
    }

    /**
     * Testcases that have been started and not completed
     * @return list of testcases that have started but not completed
     */
    public List<Map> getTestcasesStarted() { // testcases that have been started, not completed
        return this.testcasesStarted
    }

    /**
     * All testcases including setUp and tearDown
     * @return
     */
    public List<Map> getAllTestcases() {
        if (this.allTestcases == null) {
            this.allTestcases = (this.testcases + this.setUpTestcases + this.tearDownTestcases)
        }
        return this.allTestcases
    }

    /**
     * Get collection of testcases that have needed by defined
     * @return
     */
    public Map<Map, Collection> getNeededByMap() {
        return this.neededByMap
    }

    /**
     * Get depends target names as a list
     * @return list of target (not necessarily testcase) names
     */
    public Collection getDependsTargetNames(final Map testcase) {
        def groupDepends = []
        if (testcase.group != null) {
            //message 'debug', "testcase.group: ${testcase.group}"
            helper.getAsCollection(testcase.group).each { group ->
                groupDepends += getDependsTargetNamesFromNeededBy(group)
            }
        }
        return expandGroups((helper.getAsCollection(testcase.depends) + getDependsTargetNamesFromNeededBy(testcase)) + groupDepends)
    }

    /**
     * Given a collection of names, if any are group names, expand the group to names and remove group.
     * This does one level deep, perhaps we need groups with groups, etc... TODO, I don't think we support that in other parts either
     * @param list
     * @return expanded list
     */
    public Collection expandGroups(final Collection list) {
        def result = []as Set
        def groupNames = groupListMap.keySet()
        list.each { name ->
            if (name in groupNames) {
                for (element in groupListMap[name]) {
                    result << element.name
                }
            }
            result << name // always add the name in as well, even if they have a group with the same name
        }
        return result
    }

    public Collection getEndTestcaseNames(final Map testcase) {
        return helper.getAsCollection(testcase.end)
    }
    public Collection getTearDownTestcaseNames(final Map testcase) {
        return helper.getAsCollection(testcase.tearDown)
    }

    public void setPatternFlag(final flag) {
        this.patternFlag = flag
    }
    public int getPatternFlag() {
        return this.patternFlag
    }

    /**
     * Subclass can override this to set a default cmd generator automatically
     * @return current command generator
     */
    public Closure getCmdGenerator() {
        return this.defaultCmdGenerator
    }
    public Closure getCmdGenerator(final String name, final Map parameters = null) {
        return getGintCmdGenerator().getGenerator(name, parameters)
    }

    public void setCmdGenerator(final Closure cmdGenerator) {
        this.defaultCmdGenerator = cmdGenerator
    }
    public void setCmdGenerator(final String name, final Map parameters = null) {
        if (name != null) {
            setCmdGenerator(getGintCmdGenerator().getGenerator(name, parameters))
        }
    }

    /**
     * Add or replace a cmd generator to the list of known generators
     * @param name - generator name
     * @param generator cloaure
     */
    public void addCmdGenerator(final name, final Map parameters = null, final Closure generator) {
        getGintCmdGenerator().add(name, parameters, generator)
    }

    /**
     * Customize how a missing testcase name is handled
     * @param closure
     */
    public void setAddClosure(final Closure closure) {
        this.addClosure = closure
    }

    /**
     * Customize to sort testcase list
     * @param closure
     */
    public void setSortClosure(final Closure closure) {
        this.sortClosure = closure
    }

    /**
     * Gives user access to log entry prior to it being written to the command log
     * @param cmdLog - a 1 parameter closure returning a string to be logged
     */
    public void setCmdLogClosure(final Closure cmdLog) {
        this.cmdLogClosure = cmdLog
    }

    /**
     * Set filter for logging. See Helper.setLogFilter
     */
    public void setLogFilter(final filter) {
        this.helper.setLogFilter(filter)
    }

    public Closure getLogFilter() {
        return this.helper.getLogFilter()
    }

    /**
     * Set report filter to filter out properties in the standard XML junit report
     * <pre>
     * Filter closure should take one parameter and return true if value is to be filter (not logged).
     * Filter pattern will be used as a stringContains pattern
     * Filter string will be used as a case insensitive, literal pattern
     * </pre>
     * @param filter pattern or closure returning boolean - example: { text -> return helper.stringContains(text, ~/(?i)password/) }
     */
    public void setReportFilter(final filter) {
        this.reportFilter = (
                                        (filter instanceof Closure) ? filter
                                        : (filter instanceof Pattern  ? { text ->
                                            this.helper.stringContains(text, filter) }
                                        : (filter instanceof String   ? { text ->
                                            this.helper.stringContains(text, filter, Pattern.CASE_INSENSITIVE | Pattern.LITERAL) }
                                        : null)))
    }

    /**
     * Get the report filter
     * @return closure to filter out properties in the report
     */
    public Closure getReportFilter() {
        return this.reportFilter
    }

    /**
     * Get outData for a testcase. Handles initializing to an empty list if null. Convenience for users
     * @param testcase (not null)
     * @return list of strings
     */
    public List getOutData(final Map<String, Object> testcase) {
        if (testcase?.outData == null) {
            assert testcase // testcase cannot be null
            testcase.outData = []
        }
        return testcase.outData
    }

    /**
     * Add testcase
     * @param testcase
     */
    public Map add(final Map testcase) {
        if (this.addClosure != null) {  // call custom closure for add
            this.addClosure.call(testcase)
        }
        if (testcase.name in RESERVED_NAME_LIST) {
            addTestFailedMessage("Unable to add testcase with name ${helper.quoteString(testcase.name)}. This is a reserved name.")
        }
        if (testcase.isSetUp) {
            this.setUpTestcases << testcase
        } else if (testcase.isTearDown) {
            this.tearDownTestcases << testcase
        } else {
            this.testcases.add(testcase)
        }
        this.allTestcases = null // reset for every add for getAll
        return testcase
    }

    /**
     * Add list of testcases
     * @param testcases - list of testcases
     */
    public void add(final List<Map> testcases) {
        testcases.each { testcase -> add(testcase) }
    }

    /**
     * Add testcase list and group target
     * @param name - group name
     * @param list of testcases
     */
    public void add(final name, final List<Map> testcaseList) {
        add(testcaseList) // add testcases to list
        if (helper.isNotBlank(name)) { // don't allow unnamed groups to be added
            if (groupListMap[name] == null) {
                groupListMap[name] = []as Set
            }
            groupListMap[name] += testcaseList
            //addTarget(name, testcaseList)  // add the target for the group name
        }
    }

    /**
     * Add testcases based on an sql selection
     * @param connection to run select statement on
     * @param select - select rows
     */
    public void add(final Sql connection, final String select) {
        if (connection == null) {
            addTestFailedMessage("Unable to add testcases, SQL connection not valid.")
        } else {
            try {
                def rows = connection.rows(select)
                if (rows.size() > 0) {
                    def columns = rows[0].keySet()
                    rows.each { row ->
                        addFromRow(row, columns)
                    }
                }
            } catch (Exception exception) {
                addTestFailedMessage("Unable to add testcases, exception running sql: " + exception.toString())
            }
        }
    }

    /**
     * Add testcases based on csv data. Use named parameters.
     * @param data - string to split
     * @param file - file to read data from if data is null
     * @param fileName - file to read data from if data, file are null
     * @param delimiter - what to split on (in regex compatible format!!!!) or special values of blank, blanks, tab, whitespace, pipe
     * @param quote - what is used to quote strings that may contain delimiter, usually double quote
     * @param eolString - how end of line is determined
     */
    public void addFromCsv(final Map parameters) {
        def rowList = helper.csvDataAsListOfRows(parameters)
        def columns = rowList[0]
        rowList.remove(0)  // ignore the csv column names
        add(rowList, columns)
    }

    /**
     * Add testcases based on csv like data
     * @param rowList - list of entries matching list of columns
     * @param columns - column list that will be paired with row values to form the testcase
     */
    public void add(final List<List<String>> rowList, final List<String> columns) {
        rowList.each { row ->
            addFromRow(row, columns)
        }
    }

    /**
     * Add a testcase based on a row elements and column list given the parameter names
     * @param row a sql row or other map like object
     * @param columns - column list that will be paired with row values to form the testcase
     * @return testcase that was added, information only for problem determination
     */
    public Map addFromRow(final List row, final List columns) {
        def testcase = [:]
        def success = true
        def numberOfColumns = [row.size(), columns.size()].min()
        for (int i = 0; i < numberOfColumns; i++) {
            def column = columns[i].trim()
            def value = row[i].trim()
            success = addColumnValue(testcase, column, value) && success // evaluate all before declaring failure
        }
        if (success) {
            if (numberOfColumns > 0) {
                if ((numberOfColumns > 1) || (row[0] != '')) {  // not blank csv row
                    add(testcase)
                }
            }
        } else {
            addTestFailedMessage("Failed adding row data: " + row + ", columns: " + columns)
        }
        return testcase
    }

    /**
     * Add a testcase based a row and column list
     * @param row - sql row or map
     * @param column list that will be paired with row values to form the map
     * @return testcase that was added, information only for problem determination
     */
    public Map addFromRow(final row, final columns) {
        def testcase = [:]
        def success = true
        columns.each { column ->
            success = addColumnValue(testcase, column, row[column]) && success // evaluate all before declaring failure
        }
        if (success) {
            if (columns.size() > 0) {
                add(testcase)
            }
        } else {
            addTestFailedMessage("Failed adding row data: " + row + ", columns: " + columns)
        }
        return testcase
    }

    /**
     * Add a column value to testcase with special interpretation of values based on column name
     * @param testcase
     * @param column - column name
     * @param value - value to be modified and added to testcase
     * @return false if there was an exception trying to evaluated the value
     */
    protected boolean addColumnValue(final Map testcase, final String column, value) {
        def result = true
        final GROOVY_CODE_ENDING = '_code'
        try {
            def lowerCaseColumn = column.toLowerCase()
            if (helper.isNotBlank(value)) {
                // recognize boolean values as boolean
                value = (value == 'true' ? true : value == 'false' ? false : value)
                // handle closures
                if (lowerCaseColumn.equals('inline') || lowerCaseColumn.endsWith('closure')) {
                    testcase[column + GROOVY_CODE_ENDING] = value // just for documentation/debugging purposes
                    testcase[column] = binding.variables.groovyShell.evaluate(" { testcase -> ${value} }")

                    // handle code
                } else if (lowerCaseColumn.endsWith(GROOVY_CODE_ENDING)) {
                    testcase[column] = value  // retain for documentation/debugging purposes
                    // strip off the code ending from the column name
                    testcase[column.substring(0, column.length() - GROOVY_CODE_ENDING.length())] = binding.variables.groovyShell.evaluate(value)

                    // everything else
                } else {
                    testcase[column] = value
                }
            }
        } catch (Exception exception) {
            if (getVerbose()) {
                exception.printStackTrace()
            }
            message 'error', "Invalid value: ${helper.quoteString(value)} for column: ${column}"
            result = false
        }
        return result
    }

    public void addSetUp(final Map testcase) {
        testcase.isSetUp = true
        add(testcase)
    }
    public void addSetUp(final List<Map> testcases) {
        testcases.each { testcase -> addSetUp(testcase) }
    }
    public void addTearDown(final Map testcase) {
        testcase.isTearDown = true
        add(testcase)
    }
    public void addTearDown(final List<Map> testcases) {
        testcases.each { testcase -> addTearDown(testcase) }
    }

    /**
     * Initialize - get properties, profiles, set parameters, prepare for running tests
     * @param testName - either the name of the test or a class (usually script class) that gets converted to getClass().getName()
     * @param closure used to generate the command strings
     */
    public void initialize(final testName = 'gint') {
        this.startTime = System.currentTimeMillis()

        binding.variables.globalPreHook = []   // turn off extra gant target status logging
        binding.variables.globalPostHook = []  // turn off extra gant target status slogging
        if ((testName instanceof String) || (testName instanceof GString)) {
            setTestName(testName)
        } else {
            setTestName(testName.getClass().getName()) // lower case to ensure consistent no matter how test is started
        }
        // Get the user's private gint properties file
        binding.ant.property(file: helper.getParameterValue('gintUserProperties', binding.ant.project.properties.'user.home' + '/' + PROPERTY_FILENAME))
        loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded

        // Get properties in the current directory
        binding.ant.property(file: PROPERTY_FILENAME)
        loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded

        setSourceDirectory(helper.getFileParameterValue('sourceDirectory', SOURCE_DIRECTORY, CURRENT_DIRECTORY))
        setResourceDirectory(helper.getFileParameterValue('resourceDirectory', getSourceDirectory() + System.properties.'file.separator' + RESOURCE_DIRECTORY, CURRENT_DIRECTORY))

        setTargetDirectory(helper.getFileParameterValue('targetDirectory', TARGET_DIRECTORY, CURRENT_DIRECTORY))

        // Get properties from the target directory
        def propertyPath = getTargetDirectory() + '/' + PROPERTY_FILENAME
        if ((new File(propertyPath)).exists()) {  // try project specific properties from target first (in case filtering is used)
            binding.ant.property(file: helper.getParameterValue('gintProperties', propertyPath))
        }
        loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded

        // Get project specific properties from the resource directory after the target directory settings
        binding.ant.property(file: helper.getParameterValue('gintProperties', getResourceDirectory() + '/' + PROPERTY_FILENAME))
        loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded

        def profileList = helper.separatedStringToList(helper.getParameterValue('profiles'))
        def directories = helper.getParameterValue('directories')

        def directoryList = (directories != null) ?
                                        helper.separatedStringToList(helper.getParameterValue('directories')) :
                                        [
                                            binding.ant.project.properties.'user.home',
                                            getTargetDirectory(),
                                            getResourceDirectory(),
                                            CURRENT_DIRECTORY
                                        ]
        if (!helper.loadProfile(profileList, directoryList)) {
            addTestFailedMessage("Failed loading profiles.")
        }
        loadIncludedProperties()  // if not already loaded, load them now - check after each property file is loaded

        setEncoding(helper.getParameterValue('encoding')) // set if available
        setPrefix(helper.getParameterValue('prefix', getPrefix()))

        // these parameters are used it determining directories
        setLabel(helper.getParameterValue('label', getLabel()))
        setCompareLabel(helper.getParameterValue('compareLabel', getCompareLabel()))
        setDiffCli(helper.getParameterValue('diffCli', getDiffCli()))

        setCleanOutputDirectory(helper.getBooleanParameterValue('cleanOutputDirectory', getCleanOutputDirectory())) // do before touching outputDirectory

        setInputDirectory(helper.getFileParameterValue('inputDirectory', getResourceDirectory(), CURRENT_DIRECTORY))
        if (helper.isNotBlank(getLabel())) {
            setOutputDirectory(helper.getParameterValue('outputDirectory', COMPARE_DIRECTORY +
                                            System.properties.'file.separator' + getTestName().toLowerCase() + System.properties.'file.separator' + getLabel()))
        } else {
            setOutputDirectory(helper.getParameterValue('outputDirectory', getTargetDirectory() +
                                            System.properties.'file.separator' + OUTPUT_DIRECTORY + System.properties.'file.separator' + getTestName().toLowerCase()))
        }
        if (helper.isNotBlank(getCompareLabel())) {
            setCompareDirectory(helper.getParameterValue('compareDirectory', COMPARE_DIRECTORY +
                                            System.properties.'file.separator' + getTestName().toLowerCase() + System.properties.'file.separator' + getCompareLabel()))
        } else {
            setCompareDirectory(getOutputDirectory())
        }
        setTestReportsDirectory(helper.getParameterValue('testReportsDirectory', getTargetDirectory() + System.properties.'file.separator' + TEST_REPORTS_DIRECTORY))

        setClean(helper.getBooleanParameterValue('clean', getClean())) // needed before status properties are loaded

        // test run status is stored in a properties file named after the test and stored in either
        // the output directory (if exists) or current directory, don't create the output directory!!!!
        def propertyDirectory = (new File(getTargetDirectory()).exists()) ? getTargetDirectory() : CURRENT_DIRECTORY
        this.propertyFile = new File(propertyDirectory, getTestName().toLowerCase() + '.properties')  // test specific property file
        if (this.propertyFile.exists()) {  // if already exists, then load it
            this.properties = helper.loadProperties(propertyFile)
        } else if (!getClean()) {  // don't save status of a clean run if run status wasn't already there
            this.properties = new Properties()
        }

        // do after status properties are loaded before accessing any other properties
        setStopOnFail(helper.getBooleanParameterValue('stopOnFail', getStopOnFail()))
        setNoTearDown(helper.getBooleanParameterValue('noTearDown', getNoTearDown()))
        setSimulate(helper.getBooleanParameterValue('simulate', getSimulate()))
        setIgnoreDependsResult(helper.getBooleanParameterValue('ignoreDependsResult', getIgnoreDependsResult()))
        setIgnoreDepends(helper.getBooleanParameterValue('ignoreDepends', getIgnoreDepends()))
        setVerbose(helper.getBooleanParameterValue('verbose', getVerbose() || helper.isVerbose()))
        setLevel((helper.getParameterValue('level')?.equalsIgnoreCase(INCLUDE_ALL) ? Integer.MAX_VALUE : helper.getIntegerParameterValue('level', getLevel())))
        setIncludeLevels(helper.getParameterValue('includeLevels', getIncludeLevels()))
        setExcludeLevels(helper.getParameterValue('excludeLevels', getExcludeLevels()))
        setMaxParallel(helper.getIntegerParameterValue('maxParallel', getMaxParallel()))
        setMaxThreads(helper.getIntegerParameterValue('maxThreads', getMaxThreads())) // leave as is as as long as possible so defaults to maxParallel
        setTimeout(helper.getIntegerParameterValue('timeout', getTimeout()))
        setAutoRetry(helper.getIntegerParameterValue('autoRetry', getAutoRetry()))
        setLogFilter(helper.getParameterValue('logFilter', getLogFilter()))
        setReportFilter(helper.getParameterValue('reportFilter', getReportFilter()))

        setCmdGenerator(getCmdGenerator()) // this provides an opportunity for subclasses to override and set a default cmd generator (after initialization has been done)

        prepareForXmlReport()
        prepareForCmdLog()
        if (!helper.isQuiet()) {
            println getOutputHeader()  // leave this to end so header represents time of the start of running tests
        }
        if (getVerbose()) {
            logVerboseHeaderInfo()
        }
        setStopNow(getStopNow() || (getTestFailed() && getStopOnFail()))  // handles profile load failures
    }

    /**
     * Load property files indicated by the includePropertyFiles property or parameter
     */
    protected void loadIncludedProperties() {
        if (!includedPropertiesLoaded) {
            def includes = helper.getParameterValue('includePropertyFiles')
            //helper.logWithFormat('includes', includes)
            if (helper.isNotBlank(includes)) {
                def propertyFileList = helper.csvDataAsList(data: includes) // split comma separated list
                propertyFileList.each { name ->
                    if (name.startsWith('~/')) {  // handle user home replacement
                        name = binding.ant.project.properties.'user.home' + '/' + name.substring(2)
                    }
                    binding.ant.property(file: name)
                }
                includedPropertiesLoaded = true
            }
        }
    }

    /**
     * Prepare for XML reporting. Set xmlReport based on input parameter.
     */
    protected void prepareForXmlReport() {
        this.xmlReport = helper.getParameterValueConvertType('xmlReport', false)
        if (this.xmlReport == true) {
            this.xmlReport = getTestReportsDirectory() + System.properties.'file.separator' + getTestName().toLowerCase() + '.xml' // default name
        } else if (this.xmlReport instanceof String) {
            def file = new File(this.xmlReport)
            if (file.isDirectory() || (!file.exists() && !this.xmlReport.contains('.'))) { // directory or already exists or name looks like a file name
                if (!file.exists()) {
                    binding.ant.mkdir(dir: this.xmlReport)
                }
                this.xmlReport += System.properties.'file.separator' + getTestName().toLowerCase() + '.xml'
            }
        }
    }

    /**
     * Prepare for DB reporting. Get connection based on dbReport parameter.
     * Initializing early will kill test early but leave connection open for a long time without activity, so do late connection.
     */
    protected boolean prepareForDbReport() {
        dbReport = helper.getParameterValue('dbReport', null)

        //message 'debug', 'dbReport: ' + dbReport
        //message 'debug', 'dbReport class: ' + dbReport?.getClass()?.getName()

        if (dbReport instanceof String || dbReport instanceof GString) {
            dbReport = dbReport.trim()
            if (helper.isNotBlank(dbReport)) {
                def sqlHelper = initParameters?.sqlHelper ?: binding?.variables?.sqlHelper   // reuse existing helper if available
                binding.sqlHelper = (sqlHelper instanceof SqlHelper) ? sqlHelper : new SqlHelper(binding, initParameters)

                try {
                    dbReportConnection = binding.sqlHelper.getConnectionFromProfile(dbReport)
                } catch (SQLException exception) {
                    addTestFailedMessage('Problem preparing for the DB report. Exception: ' + exception.getMessage())
                }
                catch (Exception exception) {
                    addTestFailedMessage('Problem with dbReport setup or configuration. Exception: ' + exception.getMessage())
                }
                //message 'debug', 'dbReportConnection: ' + dbReportConnection
            }
        }
        return dbReportConnection != null
    }

    /**
     * Prepare for cmd logging
     * @return
     */
    protected prepareForCmdLog() {
        def cmdLogName = helper.getParameterValue('cmdLog', null)
        if (cmdLogName != null) {
            cmdLogName = cmdLogName.trim()
            if (cmdLogName == '') { // cmdLog requested, but no name given
                cmdLogName = getOutputFile('log') // default name
            } else {
                this.cmdLog = new File(cmdLogName)
                if (cmdLog.isDirectory()) {
                    this.cmdLog = new File(cmdLogName + System.properties.'file.separator' + 'log.txt')
                }
            }
            try {
                this.cmdLog = new File(cmdLogName)
                this.cmdLog.write('')  // clear
            } catch (FileNotFoundException exception) {
                message 'warning', "Command logging disabled, problems with ${cmdLogName}. " + exception.toString()
                this.cmdLog = null
            }
        }
    }

    protected String getOutputHeader() {
        return "\n= = = = = =   " + getTestName() + ' started at ' + new Date() + "   = = = = = =\n"
    }
    protected String getOutputFooter() {
        return "\n= = = = = =   " + getTestName() + ' completed at ' + new Date() + " = = = = = ="
    }

    protected logVerboseHeaderInfo() {
        helper.logWithFormat('version', getGintVersion())
        helper.logWithFormat('stopOnFail', getStopOnFail())
        helper.logWithFormat('clean', getClean())
        helper.logWithFormat('noTearDown', getNoTearDown())
        helper.logWithFormat('level', getLevel())
        helper.logWithFormat('includeLevels', getIncludeLevels())
        helper.logWithFormat('excludeLevels', getExcludeLevels())
        helper.logWithFormat('maxParallel', getMaxParallel())
        helper.logWithFormat('maxThreads', getMaxThreads())
        if (getAutoRetry() > 0) {
            helper.logWithFormat('autoRetry', getAutoRetry())
        }
        if (getLabel() != '') {
            helper.logWithFormat('label', getLabel())
        }
        if (getCompareLabel() != '') {
            helper.logWithFormat('compareLabel', getCompareLabel())
        }
        helper.logWithFormat('directories', directories)
    }

    /**
     * Do final processing to get ready to run tests. This creates targets for all testcases. Ensures no duplicate names.
     */
    public void finalizeTest() {
        try {
            this.testcases = this.testcases.sort(this.sortClosure)  // sort testcase list before adding targets
            initializeNeededBy(getAllTestcases())
            addTarget(getAllTestcases()) // getAllTestcases ensures allTestcases variable is initialized
            groupListMap.each { name, list ->
                // add group target for each group defined
                addTarget(name, list.sort(this.sortClosure))
            }
            addCommandListTarget()
        } catch (Exception exception) {
            if (getVerbose()) {
                exception.printStackTrace()
            }
            addTestFailedMessage("Finalize test failed: " + exception.toString())
        }
        def names = []as Set
        def duplicates = []
        getAllTestcases().each { testcase ->
            // ensures allTestcases variable is initialized
            if (testcase.name in names) {
                duplicates << testcase.name
            } else {
                names << testcase.name
            }
        }
        if (duplicates.size() > 0) {
            addTestFailedMessage("Duplicate testcase names found: " + duplicates)
        }
        setStopNow(getStopNow() || (getTestFailed() && getStopOnFail()))  // stop test if settings indicate
    }

    /**
     * Initialize list of needed by testcases - fill in this.neededBy
     * @param list of all testcases
     */
    protected void initializeNeededBy(final Collection<Map> list) {
        list.each { testcase ->
            if ((testcase.neededBy instanceof Boolean) && testcase.neededBy) {
                this.neededByMap[testcase] = true
            } else if ((testcase.neededBy instanceof String) || (testcase.neededBy instanceof GString)) {
                this.neededByMap[testcase] = [
                    testcase.neededBy.toString()
                ]
            } else if (testcase.neededBy instanceof Collection) {
                this.neededByMap[testcase] = testcase.neededBy
            }
        }
    }

    protected Collection<String> getDependsTargetNamesFromNeededBy(boolean targetName) {
        message 'warning', 'Ignore boolean value used for testcase or target dependency.'
    }

    /**
     * Let list of depends target names for a target based on needed by information
     * @return list of testcase names found from the neededBy information that this testcase needs to depend on
     */
    protected Collection<String> getDependsTargetNamesFromNeededBy(final String targetName) {
        def result = []
        getNeededByMap().each { testcase, value ->
            if (((value == true) || (targetName in value)) && (targetName != testcase.name)) { // value is either true or a list of names
                result << testcase.name
            }
        }
        //message 'debug', "targetName: ${targetName}, result: ${result}"
        return result
    }

    /**
     * Let list of depends target names for a testcase based on needed by information.
     * @return list of testcase names found from the neededBy information that this testcase needs to depend on
     */
    protected Collection<String> getDependsTargetNamesFromNeededBy(final Map testcase) {
        // prevent looping dependencies by avoiding setting when testcase.neededBy is also true
        return ((testcase.isSetUp == true) || (testcase.neededBy == true)) ? []: getDependsTargetNamesFromNeededBy(testcase.name)
    }

    /**
     * Run (start) the testcase after checking for proper ending of other testcases
     * @param testcase
     * @param requestEndStarted - true to end all previously started testcases
     * @parma isTearDown - true to treat as a tear down
     * @return true if testcase was actually started
     */
    public boolean runTestcase(final Map testcase, final boolean requestEndStarted = false, final boolean isTearDown = false) {

        def endStarted = getStopNow()  // if true, end all and don't start any new ones
        def startMessage = true
        if (!endStarted) { // && !isSkipped(testcase.level, testcase)) don't think we should check this now, wait for later check
            if (requestEndStarted || (getMaxParallel() < 2)) { // not doing parallel, don't need message
                endStarted = true
                // startMessage = requestEndStarted - always do a start message to bound any output logged !!
            } else {
                if ((true == testcase.end) && (this.testcasesStarted.size() > 0)) {
                    if (getVerbose()) {
                        message 'info', "${testcase.name} requested end of all started testcases"
                    }
                    endStarted = true
                }
            }
            if (!endStarted) {  // may need to end some named testcases
                if ((testcase.end != null) || (testcase.depends != null) || (this.neededByMap.size() > 0)) {
                    endCompletedTestcases() // end testcases that have completed anyway
                    def nameList = []as Set
                    (getEndTestcaseNames(testcase) + getDependsTargetNames(testcase)).each { name ->
                        nameList << name.toString()  // eliminate GStrings in the list
                        if (name in groupListMap.keySet()) {
                            groupListMap[name].each { localTestcase ->
                                nameList << localTestcase.name
                            }
                        }
                    }
                    def moreToProcess = true  // need to be careful with restart case
                    def started = new ArrayList(this.testcasesStarted) // need copy of list as it will change
                    started.each { startedTestcase ->
                        if (startedTestcase.name in nameList) {
                            if (getVerbose()) {
                                message 'info', "${startedTestcase.name} is being forced to complete by ${testcase.name}\n"
                            }
                            if (startedTestcase in this.testcasesStarted) {  // it may have ended when the previous end occurred
                                endTestcaseHandleRestart(startedTestcase) // make sure this is ended and not just restarted
                            }
                        }
                    }
                }
                def needThreadEnded = (testcase.inline != null) && (this.activeThreadCount >= getMaxThreads())
                if ((this.testcasesStarted.size() >= getMaxParallel()) || needThreadEnded) {
                    endCompletedTestcases(1, needThreadEnded) // end at least 1 until we are below both limits
                }
            }
        }
        if (endStarted) {
            endStartedTestcases() // end all started testcases
        }
        def result = false

        if (!getStopNow()) {  // not suppose to stop already
            if (!isTearDown && shouldSkip(testcase)) {
                if (!getStopNow() && !getSimulate()) {  // shouldSkip may have determined that test should stop
                    handleSkipTestcase(testcase, false, true)
                }
            } else {
                startTestcase(testcase, startMessage, isTearDown)
                result = true
            }
        }
        return result
    }

    /**
     * Check (depending on settings) if we should skip testcase because depends testcase may have been skipped or failed
     * @param testcase
     * @return true if the testcase should be skipped
     */
    protected boolean shouldSkip(final testcase) {
        def skipIt = false // may need to skip because depends testcase failed or was skipped
        def dependsTestcase // testcase listed on depends statement
        if (testcase.depends != null) {
            if ((testcase.ignoreDependsResult == false) || ((testcase.ignoreDependsResult == null) && !getIgnoreDependsResult())) {
                getDependsTargetNames(testcase).each { dependsName ->
                    if (!skipIt) {
                        dependsTestcase = findTestcase(dependsName)
                        // skip if dependsTestcase is null (not a testcase) or it was unsuccessful
                        skipIt = (dependsTestcase != null) && ((dependsTestcase.success != true) || (dependsTestcase.didFail == true)) // dependsName may be a regular target, that is ok
                        if (skipIt) {
                            if (getVerbose()) {
                                message 'info', "${testcase?.name} had a ${(dependsTestcase.success == false) ? 'failed' : (dependsTestcase.didFail == true) ? 'failed (ignored)' : 'skipped'} depends testcase ${dependsName}"
                            }
                            if (dependsTestcase.success == false) {
                                checkStopConditions(testcase)
                            }
                        }
                    }
                }
            }
        }
        return skipIt
    }

    /**
     * Start the testcase process or thread running.  Make sure any depends testcases have completed.
     * Note that all depends testcases/targets should have at least been started due to the GANT dependency mechanism,
     * we just need to worry about them being completed.
     * @param testcase
     * @param startMessage - true to log a start message
     * @param isTearDown - is to be considered a tearDown testcase
     */
    protected void startTestcase(final Map testcase, final boolean startMessage, final boolean isTearDown = false) {
        // must wait until last minute to determine whether testcase is intended to be skipped to allow dependencies to finish
        if (isSkipped(testcase.level, testcase)) {
            handleSkipTestcase(testcase)
        } else if (getSimulate()) {
            message 'simulate', testcase.name + (testcase.description ? ' (' + testcase.description + ')' : '') + (isTearDown ? ' as tearDown' : '')
        } else {
            if (testcase.sleep instanceof Integer) {
                endCompletedTestcases() // end any completed tests before sleeping
                sleepRequest(testcase.sleep, testcase.name)
            }

            // handles conversion of cmd generator and closures to cmd string, do this as late as possible for variable resolution from other testcase closures
            getCmd(testcase) // sets testcase.cmd to string value if needed

            if ((testcase.inline instanceof Closure) || helper.isNotBlank(testcase.cmd)) {  // a valid testcase
                if (startMessage) {
                    message 'start', testcase.name + (testcase.description ? ' (' + testcase.description + ')' : '') + (isTearDown ? ' as tearDown' : '')
                    //org.codehaus.groovy.runtime.StackTraceUtils.sanitize(new Exception()).printStackTrace()
                }
                if ((testcase.cmd != null) && (testcase.cmdLog != false) && this.cmdLog && (isExpectedResult(0, testcase.expected))) { // only log commands expected to be successful
                    addCmdLog(testcase.cmd)
                }
                if ((testcase.im == null) || getGintIm().getResult(testcase)) { // before IM
                    this.testcasesStarted << testcase
                    testcase.startTime = System.currentTimeMillis()
                    def shouldRun = true
                    if (testcase.startClosure) {
                        def result = runClosure(testcase.startClosure, testcase, 'startClosureResult', 'startClosureResult') // stop run only if start closure returns false
                        shouldRun = result != false
                        // helper.logWithFormat('testcase', testcase)
                        if ([
                            FAIL_TIMEOUT,
                            FAIL_ERROR,
                            FAIL_ASSERTION,
                            FAIL_EXCEPTION,
                        ].contains(testcase.startClosureResult)) {
                            setTestFailed()  // error message already reported, this will error out the entire test
                        }
                    }
                    if (shouldRun) {
                        // Run the inline or start the command
                        if (testcase.inline != null) {
                            handleInline(testcase)
                        } else {
                            testcase.parallel = (testcase.shell == false) ?
                                                            this.parallel.executable([standardInput: helper.getValueHandleClosure(testcase.standardInput)], testcase.cmd) :
                                                            this.parallel.shell([standardInput: helper.getValueHandleClosure(testcase.standardInput)], testcase.cmd)
                        }
                        if (testcase.complete == true) {  // need to end this one before starting others
                            endCompletedTestcases() // end testcases that have completed anyway
                            endTestcaseHandleRestart(testcase)
                        }
                    } else {
                        this.testcasesStarted.remove(testcase)
                        testcase.level = false
                        handleSkipTestcase(testcase, true, true)
                    }
                } else if (testcase.imResult?.skip == true) {
                    handleSkipTestcase(testcase, false, true)
                } else {  // im before says it should be considered failed
                    getTestcasesRun() << testcase   // marks this as completed for summary
                    testcase.success = false
                    testcase.result = -1
                    if (!isTearDown) { //  || (testcase.ignoreFailure == false)) {
                        handleResultMessage(testcase)
                    } else if (!testcase.success && getVerbose()) {
                        message 'ignore', "${testcase.name} failed - ignored for tearDown or if ignoreFailure requested"
                    }
                }
            } else {
                addTestFailedMessage("Testcase '${testcase.name}' does not define cmd or inline code.")
            }
        }
    }

    /**
     * Sleep for specified length and handle logging.
     * @param length
     * @param testcase
     */
    protected void sleepRequest(final sleepTime, final name) {
        if (sleepTime > 0) {
            if (getVerbose()) {
                message 'info', sleepTime + ' milliseconds sleep requested by testcase ' + name
            }
            sleep(sleepTime)
        }
    }

    /**
     * Handle skipping a testcase - for reporting later
     * @param testcase
     */
    protected void handleSkipTestcase(final testcase, final reportSkipLevel = true, final showSkipMessage = false) {
        def skipLevel = convertLevel(testcase.level, testcase)
        //if (!testcase.isSetUp && !testcase.isTearDown) { // don't count setup or tearDown testcases as really skipped, messes up counts
        this.testcasesSkipped << testcase
        if (reportSkipLevel) {
            this.skippedLevels << skipLevel
        }
        //}
        if (showSkipMessage || getVerbose()) {
            message 'info', ("Skip testcase: " + testcase.name //
                                            + (reportSkipLevel ? ", level: " + skipLevel : '') //
                                            + (testcase.startClosureResult == null ? '' : ', start closure result: ' + testcase.startClosureResult))
        }
    }

    /**
     * Test to see if this testcase is to be skipped (not run)
     * <pre>
     * - excludeLevels takes precedence over other values, ie if match exclude, then skip
     * - includeLevels takes precedence over integer levels except for all case, then integer level is still used
     * - for collections, if all entries are to be skipped, then skip otherwise include
     * </pre>
     * @param inLevel - level to check, usually testcase.level
     * @param testcase
     * @return true if testcase is intended to be skipped
     */
    public boolean isSkipped(final inLevel, final Map testcase) {
        def level = convertLevel(inLevel, testcase)
        if (level instanceof Collection) {
            for (entry in level) {  // use for so we can do quick return
                if (!isSkipped(entry, testcase)) {
                    return false  // return on first none skipped entry
                }
            }
            return true // all entries were skipped, so skip this
        }
        if (level in getExcludeLevels()) {
            return true
        }
        if (getIncludeLevels()) {
            if (level instanceof Integer) {
                return (level > this.level) && !(level in getIncludeLevels())
            }
            return !(level in getIncludeLevels())
        } else if (level instanceof Integer) {
            return (level > this.level)
        }
        return false // null include levels list means include ALL
    }

    /**
     * Calculate the actual level from the input level. In particular convert null to 1 and evaluate closures.
     * @param level to convert, default the testcase level, separate parameter needed for recursion
     * @param testcase needed as parameter to closure if level is a closure
     * @return
     */
    protected convertLevel(final level, final Map testcase) {
        try {
            return (level == null) ? 1 :
            (level instanceof Closure ?
            ((level.getMaximumNumberOfParameters() > 0) ? level(testcase) : level()) : level)
        } catch (Exception exception) {
            if (getVerbose()) {
                message 'warning', "Exception in level closure ignored. " + exception.toString()
            }
            return 1
        }
    }

    /**
     * Adds to the command log, user can customize using the cmdLogClosure
     * @param entry to be added to the log
     */
    public void addCmdLog(final entry) {
        if (cmdLog) {
            cmdLog << cmdLogClosure.call(entry)
        }
    }

    /**
     * Get the command to use for a testcase. Use explicit testcase cmd if provided,
     * otherwise generate it from the default command generator.
     * Only evaluate once, so save result in testcase.cmd for use later.
     * @param testcase
     * @return cmd string or null
     */
    protected getCmd(final Map testcase) {
        if (testcase.cmd == null) {
            testcase.cmd = ((testcase.inline != null) ? null :
                                            (testcase.cmd ?: ((testcase.cmdGenerator instanceof Closure) ? testcase.cmdGenerator(testcase) :
                                            (this.defaultCmdGenerator ? this.defaultCmdGenerator(testcase) : null))))
        } else if (testcase.cmd instanceof Closure) {
            testcase.cmd = (testcase.cmd.getMaximumNumberOfParameters() > 0) ? testcase.cmd(testcase) : testcase.cmd()
        }
        return testcase.cmd
    }

    /**
     * Handle inline testcase. Start a thread for the testcase. It will be ended later.
     * @param testcase - is updated with result data
     */
    protected void handleInline(Map testcase) {
        def retryCount = 0
        while (retryCount < 2) { // try twice if there is a random failure
            testcase.thread = Thread.start {
                // closure should return true, false or a integer return code, null is considered as successful
                // give thread chance to start before starting another to avoid occasional errors caused by http://jira.codehaus.org/browse/GROOVY-3495
                //sleep(this.activeThreadCount * 15) // even with this, it can still fail
                try {
                    // if inline isn't a closure already, make it a closure!
                    def  inline = (testcase.inline instanceof Closure) ? testcase.inline : binding.variables.groovyShell.evaluate(" { testcase -> ${testcase.inline} }")
                    testcase.result = inline.call(testcase)
                    if ((testcase.result == null) || (testcase.result == true)) {
                        testcase.result = 0
                    } else if (testcase.result == false) {
                        testcase.result = -1
                    }
                } catch (Exception exception) {
                    message 'error', testcase.name + ': ' + exception.toString()
                    testcase.result = FAIL_EXCEPTION
                    getOutData(testcase) << exception.getMessage()
                    getOutData(testcase) << exception.getStackTrace().toString()
                    if (getVerbose()) {
                        exception.printStackTrace()
                    }
                } catch (Error error) {  // primarily for assertion failures or other errors
                    message 'error', testcase.name + ': ' + error.toString()
                    testcase.result = FAIL_ASSERTION
                    getOutData(testcase) << error.getMessage()  // log the assertion failure data
                    if (!error.toString().contains('assert')) { // don't do assert stack traces - just noise
                        getOutData(testcase) << error.getStackTrace()
                        if (getVerbose()) {
                            error.printStackTrace()
                        }
                        testcase.result = FAIL_ERROR
                    }
                }
            }
            if (testcase.thread == null) {  // should not happen but randomly does (rarely), must be a Groovy bug on 1.7.x ??
                retryCount++
                message 'warning', "Testcase thread is null. This should not happen!" + (retryCount < 2) ? ' Retry.' : ''
            } else {
                retryCount = 99
            }
        }
        this.activeThreadCount++
    }

    /**
     * End the testcases that have completed. If requested, ensure at least the needed number of testcases are ended before returning.
     * @param neededToEnd - number of testcases that need to be ended before returning control to caller - loop until done!
     * @param needThreadEnded - set to true will force at least one threaded testcase to be ended
     */
    protected void endCompletedTestcases(final int neededToEnd = 0, final needThreadEnded = false) {
        def endedCount = 0
        def endedThread = false
        def waitMessageSent = !getVerbose() // send wait message at most once (don't send if verbose is off)
        def moreToProcess = true  // need to be careful with restart case
        while (moreToProcess) {
            moreToProcess = false
            def started = new ArrayList(this.testcasesStarted)
            while (started.size > 0) {
                started.each { testcase ->
                    if ((getMaxParallel() == 1) || isTestcaseProcessingComplete(testcase)) {
                        if (endTestcase(testcase)) {
                            endedCount++
                            endedThread = (testcase.thread != null)
                        } else {
                            moreToProcess = true
                        }
                    }
                }
                if ((endedCount < neededToEnd) || (!endedThread && needThreadEnded)) {
                    if (!waitMessageSent) {
                        def reason = (this.testcasesStarted.size() >= getMaxParallel()) ? "Max parallel limit (${getMaxParallel()})" : "Max thread limit (${getMaxThreads()})"
                        message 'info', reason + " reached. Wait for a testcase to complete.\n"
                        waitMessageSent = true
                    }
                    sleep(100) // wait for a bit
                }
                if (endedCount >= neededToEnd) {
                    break // leave loop now
                }
            }
        }
    }

    /**
     * Check to see if testcase processing has completed
     * @param testcase
     * @return true if the testcase has finished processing
     */
    protected boolean isTestcaseProcessingComplete(final Map testcase) {
        return (testcase.parallel && testcase.parallel.isComplete()) ||
        (testcase.thread && (testcase.thread.getState() == Thread.State.TERMINATED)) ||
        hasExceededTimeout(testcase)
    }

    /**
     * Has the testcase been running to long?
     * @param testcase
     * @return true if timeout value is defined and exceeded
     */
    protected boolean hasExceededTimeout(final Map testcase) {
        def timeout = (testcase.timeout instanceof Integer) ? testcase.timeout : getTimeout()
        return (timeout instanceof Integer) && (timeout > 0) && (timeout < (System.currentTimeMillis() - testcase.startTime))
    }

    /**
     * End processing for testcase, assumes that testcase is ready to be ended
     * @param testcase
     */
    protected void endTestcaseProcessing(final Map testcase) {
        def done = (testcase.timeout ?: getTimeout()) <= 0  // has process been ended or we can wait
        def timeoutMessage = null  // try normal end first
        while (true) {
            if (testcase.thread) {
                done = done || (testcase.thread.getState() == Thread.State.TERMINATED)
                if (done) {
                    testcase.thread.join() // assumes the testcase sets the result and any other data that is required
                    testcase.result = testcase.result ?: 0
                } else if (timeoutMessage) {
                    done = true
                    testcase.result = FAIL_TIMEOUT
                    testcase.failReason = timeoutMessage
                    testcase.thread = null  // abandon the thread, it will continue to run until done or JVM ends
                }
                if (done) {
                    this.activeThreadCount--
                }
            } else if (testcase.parallel) {
                done = done || testcase.parallel.isComplete()
                if (done) {
                    testcase.result = testcase.parallel.end(false)  // end testcase but don't print yet
                } else if (timeoutMessage) {
                    done = true
                    testcase.parallel.getProcess().destroy()
                    testcase.result = FAIL_TIMEOUT
                    testcase.failReason = timeoutMessage
                }
            } else {
                break // abnormal to had both thread and parallel null, consider it ended to prevent any looping
            }
            if (done) {
                break  // break before sleep
            }
            if (hasExceededTimeout(testcase)) {
                timeoutMessage = "${testcase.timeout ?: getTimeout()} millisecond timeout exceeded"
            } else {
                sleep(100) // wait for a bit
            }
        }
    }

    /**
     * End the testcase process and check all provided conditions:
     * - command result matches expected
     * - data is found in output
     * - failData is NOT found in output
     * - resultClosure returns true
     * @param testcase
     * @param request ignore failure - force ignore failure on call, used for testcase specific tearDown actions
     * @return true if testcase was ended or false if testcase had to be restarted
     */
    protected boolean endTestcase(final Map testcase, final boolean requestIgnoreFailure = false) {
        // ignore errors for tearDown or explicitly requested for a testcase
        def ignoreFailure = requestIgnoreFailure || (testcase.isTearDown == true)
        def ignoreFailure2 = ignoreFailure || (testcase.ignoreFailure == true)
        if (ignoreFailure2 || (getMaxParallel() > 1)) { // we we need a message
            message 'ending', testcase.name + (testcase.description ? ' (' + testcase.description + ')' : '') + (ignoreFailure2 ? ' ignoring any failure' : '')
        }

        this.testcasesStarted.remove(testcase)  // remove this testcase from the started list now that it has been ended

        if ((testcase.parallel == null) && (testcase.thread == null) && (testcase.im == null)) {
            def msg = ((testcase.inline == null) ? "Process" : "Thread") + " is null for testcase ${testcase.name} for ending testcase. This is not expected."
            addTestFailedMessage(msg)
            testcase.success = false
            if (helper.isDebug()) {
                Thread.dumpStack()  // need to understand why this abnormal case is happening
            }
            return true   // no restart
        }
        try {
            endTestcaseProcessing(testcase)   // determines testcase.result
            testcase.endTime = System.currentTimeMillis()
        } catch (Exception exception) {
            testcase.endTime = System.currentTimeMillis()
            if (getVerbose()) {
                exception.printStackTrace()
            }
            addTestFailedMessage("Exception running testcase ${testcase.name}.")
            testcase.success = false
            return true   // no restart
        }

        setOutData(testcase)  // make sure this is set before the result closure is called!

        // Now a chance for testcase specific processing to update/change the result
        if (testcase.resultClosure != null) {
            runClosure(testcase.resultClosure, testcase, "result", "result")  // testcase result is updated with closure result or closure exception info
            testcase.result = testcase.result ?: 0  // map null or false result to 0
        }

        // handle both windows and non-windows result codes- windows uses -1, -2, etc... that gets converted to positive number on unix via 256 + number.
        //testcase.success = ((testcase.result == testcase.expected) || (testcase.result == (256 + testcase.expected)))
        testcase.success = isExpectedResult(testcase.result, testcase.expected, testcase.name)

        def restartNeeded = false
        //if (!ignoreFailure) { // skip this for tearDown operations - don't mess up data for valid runs of the testcase
        if (!requestIgnoreFailure) {  // determine outcome for all runs except testcase specific teardown requests
            determineOutcome(testcase)
            restartNeeded = testcase.restartRequested
        }
        testcase.endTime = System.currentTimeMillis() // recalculate endtime due to result or success closure testscases

        // Log error information before attempting restart processing
        logOutputAfterEnd(testcase, ignoreFailure) // opportunity to log testcase output based on parameters or other factors
        if (!requestIgnoreFailure || (testcase.ignoreFailure != true) || (testcase.isTearDown != true)) {
            handleResultMessage(testcase, ignoreFailure2 || restartNeeded)
        } else if (!testcase.success && getVerbose()) {
            message 'ignore', "${testcase.name} failed - ignored for tearDown or if ignoreFailure requested"
        }

        if (restartNeeded == true) {  // restart and don't do any more processing here
            restartTestcase(testcase)
        } else {
            //            logOutputAfterEnd(testcase, ignoreFailure) // opportunity to log testcase output based on parameters or other factors
            //            if (!requestIgnoreFailure || (testcase.ignoreFailure != true) || (testcase.isTearDown != true)) {
            //                handleResultMessage(testcase, ignoreFailure2)
            //            } else if (!testcase.success && getVerbose()) {
            //                message 'ignore', "${testcase.name} failed - ignored for tearDown or if ignoreFailure requested"
            //            }
            // final closure is run after the testcase results have been calculated and reported
            // Note that final closure may run for tearDown, but no testcase result data is available
            if ((testcase.finalClosure != null) && (!runClosure(testcase.finalClosure, testcase))) {
                setTestFailed() // problems with finalClosures failing must be reported as global test failures as they occur after testcase has completed
                return true // final closure experienced an exception - end no matter what
            }
            testcase.didFail = !testcase.success  // indicator that remembers failure status, even though we may be ignoring that
            if (testcase.ignoreFailure == true) {
                testcase.success = true  // mark as success even if it failed - needed at least for dependency handling
            }
            message 'complete', testcase.name + ' - ' + (testcase.endTime - testcase.startTime)/1000 + ' secs' + '\n\n' // leave space after
            testcase.parallel = null // free memory
            testcase.thread = null // free memory
            testcase.outData = null // free memory

            if (testcase.sleepAfter instanceof Integer) {
                sleepRequest(testcase.sleepAfter, testcase.name)
            }

            if (testcase.startNext != null) { // TODO just handle simple case to start
                def nextTestcase = findTestcase(testcase.startNext)
                if (nextTestcase != null) { // found
                    if (okToRun(nextTestcase.name)) {
                        // only start if not already run or started
                        runTestcase(nextTestcase)
                    }
                }
            }
        }
        return !restartNeeded
    }

    /**
     * Testcase has ended and is about to report outcome. This is an opportunity to decide to log additional information.
     * Default is to log output from parallel testcases if verbose is set
     * @param testcase - testcase that has ended
     * @param ignoreFailure - ignore failure setting
     */
    protected void logOutputAfterEnd(final Map testcase, final boolean ignoreFailure = false) {
        if (getVerbose() || !(testcase.success || ignoreFailure)) {
            if (testcase.parallel) {
                testcase.parallel.printOutList()
                testcase.parallel.printErrorList()
            }
        }
    }

    /**
     * End testcase with handling of restart case. This means make sure the testcase is ended before returning
     * @param testcase
     * @return
     */
    protected void endTestcaseHandleRestart(final Map testcase, final boolean requestIgnoreFailure = false) {
        for (int i = 0; i < 1000; i++) {  // don't let this go on forever in case there is a bug, otherwise logging can fill disk space
            if (endTestcase(testcase, requestIgnoreFailure)) {
                break  // it has ended now
            }
        }
    }

    /**
     * Run the list of testcase closures
     * @param list of closures
     * @param testcase
     * @param closureResultKey - key in testcase to update with the results of the last closure
     * @return true if all closures run successfully (no exceptions), false as soon as one fails with exception
     */
    protected boolean runClosure(final List list, final Map testcase, final closureResultKey = null, final closureExceptionKey = null) {
        def result = true
        list.each { entry ->
            if (entry instanceof Closure) {
                result = result && runClosure(entry, testcase, closureResultKey, closureExceptionKey)
            }
        }
        return result
    }

    protected boolean runClosure(final closure, final Map testcase, final closureResultKey = null, final closureExceptionKey = null) {
        if (closure instanceof Closure) {
            if (getSynchronizeClosures()) {
                return runSynchronizedClosure(closure, testcase, closureResultKey, closureExceptionKey)
            } else {
                return runClosureInternal(closure, testcase, closureResultKey, closureExceptionKey)
            }
        } else {
            message 'warning', "Ignore entry that is not a closure: ${closure}"
            return false
        }
    }

    @Synchronized
    protected boolean runSynchronizedClosure(final closure, final Map testcase, final closureResultKey = null, final closureExceptionKey = null) {
        return runClosureInternal(closure, testcase, closureResultKey, closureExceptionKey)
    }

    /**
     * Run a testcase closure
     * @param closure
     * @param testcase or other similar Map parameter for the closure
     * @param closureResultKey - key in testcase to update with the results of the closure
     * @return true if closure was run successfully (no exceptions)
     */
    protected boolean runClosureInternal(final closure, final Map testcase, final closureResultKey = null, final closureExceptionKey = null) {
        def result = false
        try {
            if (closureResultKey != null) {
                testcase[closureResultKey] = closure.call(testcase)
            } else {
                closure.call(testcase)
            }
            result = true

        } catch (Exception exception) {
            if (getVerbose()) {
                exception.printStackTrace()
            }
            addTestFailedMessage("Closure for testcase ${testcase.name} generated exception: " + exception.toString(), false) // but don't fail the test completely
            if (closureExceptionKey != null) {
                testcase[closureExceptionKey] = this.FAIL_EXCEPTION
            }

        } catch (Error error) {  // primarily for assertion failures or other errors
            addTestFailedMessage("Closure for testcase ${testcase.name} generated error: " + error.toString(), false) // but don't fail the test completely
            handleError(testcase, error, closureExceptionKey)
        }
        return result
    }

    protected void handleError(final Map testcase, final Error error, final closureExceptionKey = null) {
        getOutData(testcase) << error.getMessage()  // log the assertion failure data
        if (!error.toString().contains('assert')) { // don't do assert stack traces - just noise
            getOutData(testcase) << error.getStackTrace()
            if (getVerbose()) {
                error.printStackTrace()
            }
            if (closureExceptionKey != null) {
                testcase[closureExceptionKey] = this.FAIL_ERROR
            }
        } else {
            if (closureExceptionKey != null) {
                testcase[closureExceptionKey] = this.FAIL_ASSERTION
            }
        }
    }


    /**
     * For a testcase that has been ended, do all the calculation to determine the outcome.
     * Set appropriate global and testcase fields
     * @param testcase
     */
    protected void determineOutcome(final Map testcase) {

        // look for expected data
        def outDataAsString = null
        def patternFlag = ((testcase.patternFlag != null) ? testcase.patternFlag : getPatternFlag())
        if (testcase.success && testcase.data) {
            // enable multi-line pattern matching
            outDataAsString = helper.listToSeparatedString(getOutData(testcase), System.properties.'line.separator')
            testcase.found = helper.verifySearchInTarget(search: testcase.data, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: testcase)
            testcase.success = testcase.found
        }

        // look for fail data
        if (testcase.success && testcase.failData) {
            if (outDataAsString == null) {
                outDataAsString = helper.listToSeparatedString(getOutData(testcase), System.properties.'line.separator')
            }
            testcase.notFound = helper.verifySearchNotInTarget(search: testcase.failData, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: testcase)
            testcase.success = testcase.notFound
        }

        // look in generated output for data or fail data, if a file name is not specified, use the default file name based on testcase name
        if (testcase.success && (testcase.output instanceof Map)) {
            if (testcase.output.file == null) {
                testcase.output.file = (testcase.file instanceof String || testcase.file instanceof GString) ? testcase.file : getOutputFile(testcase.name)
            }
            if ((testcase.output.file instanceof String) || (testcase.output.file instanceof GString)) {
                testcase.output.exists = new File(testcase.output.file).exists()
                testcase.success = testcase.output.exists
            }
            if (testcase.success) {
                testcase.output.found = true  // initialize to make later result message comparison simpler
                testcase.output.notFound = true
                def patternFlag2 = ((testcase.output.patternFlag != null) ? testcase.output.patternFlag : patternFlag)
                def fileAsString = null
                if (testcase.output.data) {
                    if (fileAsString == null) {
                        fileAsString = readFile(testcase.output.file, testcase)
                    }
                    testcase.output.found = helper.verifySearchInTarget(search: testcase.output.data, target: fileAsString, patternFlag: patternFlag2, logFailure: true, firstParameter: testcase)
                    testcase.success = testcase.output.found
                }
                if (testcase.success && testcase.output.failData) {
                    if (fileAsString == null) {
                        fileAsString = readFile(testcase.output.file, testcase)
                    }
                    testcase.output.notFound = helper.verifySearchNotInTarget(search: testcase.output.failData, target: fileAsString, patternFlag: patternFlag2, logFailure: true, firstParameter: testcase)
                    testcase.success = testcase.output.notFound
                }
            }
        }

        // compare requested?
        if (testcase.success && ((testcase.compare instanceof Map) || (testcase.compare == true))) {
            testcase.compareSuccess = runDiff(testcase)   // run file compares
            testcase.success = testcase.compareSuccess
        }
        // image compare requested?
        if (testcase.success && ((testcase.imageCompare instanceof Map) || (testcase.imageCompare == true))) {
            testcase.compareSuccess = runImageCompare(testcase)   // run image compares
            testcase.success = testcase.compareSuccess
        }

        boolean doRetry = false
        // im response?
        if (testcase.im) {
            def result = getGintIm().getResult(testcase, false)  // after running testcase
            if (testcase.restartRequested == true) {
                return // don't do anything more here!!!
            }
            testcase.success = testcase.success && result

            // retry processing only if not im - just to simplify it a bit
        } else if (isRetryConfigured(testcase)) {
            // testcase was not successful or it is successful but the expected result is not correct
            if (!testcase.success || !isExpectedResult(testcase.result, testcase.expected)) {
                doRetry = shouldDoRetry(testcase, outDataAsString)
            }
        }
        if (!doRetry) {
            // if successful so far, run the successClosure if it has one
            if (testcase.success && (testcase.successClosure != null)) {
                runClosure(testcase.successClosure, testcase, "closureResult") // testcase closureResult is updated with closure result
                testcase.closureResult = testcase.closureResult ?: false // map null to false so it will be reported properly
                testcase.success = testcase.closureResult

                // Check if we need a retry on success closure failure
                if (!testcase.success && isRetryConfigured(testcase)) {
                    doRetry = shouldDoRetry(testcase, outDataAsString)
                }
            }
        }
        if (!doRetry) {
            getTestcasesRun() << testcase // log this testcase to count in results
            // test fails if any non-ignored test fails for whatever reason
            if (!testcase.success) {
                checkStopConditions(testcase)
            }
            //message 'debug', "complete determineResults for testcase: ${testcase.name}"
        }
    }

    /**
     * See if retry has been configured for the testcase. Set retry count if not set
     * @return true if testcase indicates a retry
     */
    protected isRetryConfigured(testcase) {
        if (testcase.retry instanceof Integer) {
            // retry already set
        } else if (testcase.retry instanceof Boolean) {
            testcase.retry = testcase.retry == true ? 1 : 0
        } else if ((testcase.retryClosure instanceof Closure) || (testcase.retrySleep != null) || (testcase.retryData != null) || (testcase.retryCancelData != null)) {
            testcase.retry = 1
        } else if (getAutoRetry() > 0) {
            testcase.retry = getAutoRetry()
        } else {
            return false // do nothing if none of the above were specified
        }
        return testcase.retry > 0
    }

    protected shouldDoRetry(final Map testcase, String outDataAsString) {
        boolean doRetry = testcase.retryData == null
        if (!doRetry) {
            // check to see if we should do the retry based on retryData (just like matching on data for result)
            // all retryData must be found
            if (outDataAsString == null) {
                outDataAsString = helper.listToSeparatedString(getOutData(testcase), System.properties.'line.separator')
            }
            doRetry = helper.verifySearchInTarget(search: testcase.retryData, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: testcase)
            if (!doRetry && getVerbose()) {
                message 'info', 'Retry not done since retryData was not found in output.'
                helper.logWithFormat('retryData', testcase.retryData)
                //helper.logWithFormat('outDataAsString', outDataAsString)
            }
        }
        if (doRetry && (testcase.retryCancelData != null)) {
            // check to see if we should do cancel retry based on retryCancelData (just like matching on failData for result)
            // any retryCancelData found will cancel the retry
            if (outDataAsString == null) {
                outDataAsString = helper.listToSeparatedString(getOutData(testcase), System.properties.'line.separator')
            }
            doRetry = helper.verifySearchNotInTarget(search: testcase.retryCancelData, target: outDataAsString, patternFlag: patternFlag, logFailure: true, firstParameter: testcase)
            if (!doRetry && getVerbose()) {
                message 'info', 'Retry not done since at least one retryCancelData entry was found in output.'
                helper.logWithFormat('retryCancelData', testcase.retryCancelData)
            }
        }
        if (doRetry) {  // still doing retry, give retry closure a chance to change things before retry
            if (testcase.retryClosure instanceof Closure) {
                runClosure(testcase.retryClosure, testcase, "retryClosureResult") // testcase retryClosureResult is updated with closure result
                testcase.retryClosureResult = testcase.retryClosureResult ?: false // map null to false so it will be reported properly
                doRetry = testcase.retryClosureResult == true // only continue retry processing if it returns true
                if (!doRetry && getVerbose()) {
                    message 'info', 'Retry not done since retry closure did not return true.'
                }
            }
        }
        testcase.restartRequested = false // make sure this is set correctly
        if (doRetry) { // match on data condition
            if (!(testcase.restartCount instanceof Integer)) {  // initialize it if not already set to an integer
                testcase.restartCount = 0
            }
            testcase.restartRequested = (testcase.restartCount < testcase.retry)
            testcase.restartCount++
        }
        return testcase.restartRequested == true
    }

    /**
     * Read a file respecting testcase and global encoding setting
     * @param file
     * @param testcase
     * @return file contents or output.file contents or ''
     */
    public String readFile(final file = null, final Map testcase) {
        if (file == null) {  // default to the output file if it exists
            return (testcase.output?.file == null) ? '' : helper.readFile(testcase.output.file, getEncoding(testcase))
        }
        return helper.readFile(file, getEncoding(testcase))
    }

    /**
     * Read a file as list of lines respecting testcase and global encoding setting
     * @return list of lines
     */
    public List<String> readFileToList(final file = null, final Map testcase) {
        if (file == null) {  // default to the output file if it exists
            return (testcase.output.file == null) ? '' : helper.readFileToList(testcase.output.file, getEncoding(testcase))
        }
        return helper.readFileToList(file, getEncoding(testcase))
    }

    /**
     * Restart a testcase - handle restart requested
     * @param testcase
     */
    protected void restartTestcase(final Map testcase) {
        //if (getVerbose()) {
        message 'info', "Restarting testcase ${testcase.name}."
        //}
        def resetList = [
            'restartRequested',
            'success',
            'failReason',
            'imResult',
            //'output', // this is user provided data and should NOT be cleared - see GINT-40
            'outData',
            'parallel',
            'thread'
        ]
        resetList.each { key ->
            testcase[key] = null
        }
        // sleep before running it again, default to 1 sec for non im testcases
        sleepRequest(testcase.retrySleep instanceof Integer ? testcase.retrySleep : testcase.im ? 0 : 1000, testcase.name)
        runTestcase(testcase)  // start the test again
    }

    /**
     * Mark to stop processing if parameters indicate.  This assumes the testcase has failed for some reason.
     * a) testcase explicitly asked to stop on failure
     * b) stopOnFail is set but testcase did not override the setting by specifying to NOT stop on fail
     * c) testcase is a setup testcase and did not specify to NOT stop on fail
     * @param failed - current
     * @param testcase
     * @return true if testcase processing should stop because of stopOnFail conditions
     */
    protected void checkStopConditions(final Map testcase) {
        if ((testcase.ignoreFailure != true) && !testcase.isTearDown) {
            setTestFailed()
            setStopNow(getStopNow() || (((testcase.stopOnFail == true) || ((testcase.stopOnFail != false) && (getStopOnFail() || (testcase.isSetUp == true))))))
        }
    }

    /**
     * Set outData for the testcase to data from appropriate places. Override in sub class if necessary.
     * @param testcase
     */
    protected void setOutData(final testcase) {
        if (testcase.parallel) {
            testcase.outData = testcase.parallel.getOutList()
            testcase.outData += testcase.parallel.getErrorList() // always include error data even if success
        }
    }

    /**
     * End any testcase that has been started but not ended
     * - to be more accurate with testcase timing, end the ones that are finished first
     */
    public void endStartedTestcases() {
        def loopCount = 0
        while (this.testcasesStarted.size() > 0) {
            def completedList = []
            if (this.testcasesStarted.size() == 1) { // then just wait for it to be completed
                completedList << this.testcasesStarted[0]
            } else {
                this.testcasesStarted.each { testcase ->
                    if (isTestcaseProcessingComplete(testcase)) {
                        completedList << testcase
                    }
                }
            }
            if (completedList.size() == 0) {  // nothing finished yet!
                loopCount++
                if (helper.isDebug() && (loopCount > 600)) {
                    message 'debug', "Waiting for testcases to complete. ${this.testcasesStarted}"
                    loopCount = 0
                }
                sleep(100) // wait for a bit
            } else {
                completedList.each { testcase ->
                    if (!(testcase in getTestcasesRun())) {  // completed list is invalid once we follow a restarted path
                        endTestcase(testcase)
                    }
                }
                loopCount = 0
            }
        }
    }

    /**
     * Finalize results and do reports. This is normally called as part of finalize processing.
     */
    public void finalizeResults(final doReports = true) {
        if (this.startTime == null) {
            addTestFailedMessage('Test has not been initialized. gint.initialize() has not been run.')
        } else {
            try {
                endStartedTestcases()  // make sure everything is ended
                def endTime = System.currentTimeMillis()
                def elapsedTime = (endTime - this.startTime)/1000 // in seconds

                def counts = getResultCounts()

                if (helper.isQuiet()) {  // ensure the summary results get output even in GANT quiet mode
                    binding.ant.logger.setMessageOutputLevel(GantState.NORMAL)
                }

                if (doReports && !helper.isQuiet()) {
                    handleResultSummary(endTime, elapsedTime, counts) // reporting
                }

                if (helper.isQuiet()) {  // return logging to previous level
                    binding.ant.logger.setMessageOutputLevel(GantState.verbosity)
                }

                if ((counts.base.failed > 0) || (counts.setUp.failed > 0)) {
                    setTestFailed()
                } else if (getTestFailed()) {
                    message 'error', "Test marked failed or was incomplete due to problems with testcase definitions, adding testcases, or other reasons:"
                    testFailedMessageList.each { string -> message 'error', string }
                } else if (testFailedMessageList?.size() > 0) {
                    message 'warning', "Some assert or exception failures noted during run but they were ignored or retries were successful."
                    testFailedMessageList.each { string -> message 'warning', string }
                }
            } catch (Exception exception) {
                message 'error', "Exception finalizing result. Exception is: ${exception.toString()}"
                setTestFailed()
                if (getVerbose()) {
                    exception.printStackTrace()
                }
            }
        }
    }

    /**
     * Get summary result counts. A side effect is that the failed testcases are saved to the gint property file.
     * @return map of results with success, failed, skipped, notRun
     */
    protected Map<String, Map> getResultCounts() {
        def counts = [
            base:      [ success: 0, skipped: 0, failed: 0 ],
            setUp:     [ success: 0, skipped: 0, failed: 0 ],
            tearDown:  [ success: 0, skipped: 0, failed: 0 ],
            restarted: [ success: 0,             failed: 0 ], // tests that were retried and subsequently were successful or failed
        ]
        def failedList = [] // failed testcase names - not to include setUp or tearDown
        getTestcasesRun().each { testcase ->
            if (testcase.success || (testcase.ignoreFailure == true)) {
                if (testcase.isSetUp) {
                    counts.setUp.success++
                } else if (testcase.isTearDown) {
                    counts.tearDown.success++
                } else {
                    counts.base.success++
                }
                if ((testcase.restartCount instanceof Integer) && (testcase.restartCount > 0)) {
                    counts.restarted.success++
                }
            } else {
                if (testcase.isSetUp) {
                    counts.setUp.failed++
                } else if (testcase.isTearDown) {
                    counts.tearDown.failed++
                } else {
                    counts.base.failed++
                    failedList << testcase.name
                }
                if ((testcase.restartCount instanceof Integer) && (testcase.restartCount > 0)) {
                    counts.restarted.failed++
                }
            }
        }
        saveFailedTestcases(failedList)  // remembered in property file for next default start

        getTestcasesSkipped().each { testcase ->
            if (testcase.isSetUp) {
                counts.setUp.skipped++
            } else if (testcase.isTearDown) {
                counts.tearDown.skipped++
            } else {
                counts.base.skipped++
            }
        }
        counts.base.total = getTestcases().size()
        counts.setUp.total = getSetUpTestcases().size()
        counts.tearDown.total = getTearDownTestcases().size()

        ['base', 'setUp', 'tearDown'].each { type ->
            counts[type].notRun = counts[type].total - counts[type].success - counts[type].failed - counts[type].skipped
        }
        //helper.logWithFormat('counts', counts)
        //helper.logWithFormat('testcases size', this.testcases.size())
        return counts
    }

    /**
     * Handle test result reporting. Local log and optional xml, mail, im reports.
     */
    protected void handleResultSummary(final endTime, final elapsedTime, final Map<String, Map> counts) {

        def builder  // for mail or im report, if necessary
        def mailReport = helper.getParameterValueConvertType('mailReport', false)
        def imReport = helper.getParameterValueConvertType('imReport', false)

        if (mailReport || imReport) {
            builder = new StringBuilder()
            def messageClosure = { tag, msg ->
                def line = '         '.substring(tag.size()) + "[${tag}] ${msg}"
                builder.append('\n').append(line)
                println line
            }
            helper.setMessageClosure(messageClosure)
            message = messageClosure  // local message closure - note there is no harm in leaving this message closure set
        }

        // only need failure summary if more than 1 error or 1 error and it didn't just happen
        //if ((counts.failed > 1) || ((lastResult || helper.isQuiet()) && (counts.failed > 0)))  {
        if ((counts.base.failed > 0) && (!getStopOnFail() || getMaxParallel() > 1) && !helper.isQuiet())  {
            println ''
            message 'info', "Failure summary:"

            for (boolean doTearDown = false; ; doTearDown = true) {

                getTestcasesRun().each { testcase ->
                    if (!testcase.success //
                    && (testcase.ignoreFailure != true) //
                    && ((!doTearDown && (testcase.isTearDown != true)) || (doTearDown && (testcase.isTearDown == true)))) {
                        message 'failed', "${testcase.name + (testcase.description ? ' (' + testcase.description + ')' : '')}: ${testcase?.failReason?.substring(0, [testcase?.failReason?.size(), DISPLAY_LIMIT_SUMMARY].min())}"
                        if (testcase.failDetail && getShowFailDetail()) {
                            message 'reproduce', testcase.failDetail
                        }
                    }
                }
                if (doTearDown || !helper.isVerbose()) {  // no tear down failure messages unless verbose set on gant level
                    break
                } else {
                    println ''
                    message 'info', "Tear down failures (ignored):"
                }
            }
        }
        println '' // blank line between end of failure summary and report
        if (getStopNow()) {
            message 'info', 'Test stopped due to ' + (getStopOnFail() ? 'stopOnFail request.' : ((counts.setUp.failed > 0) ? 'setUp failure.' : 'a specific request.'))
        }

        // Normal counts reporting
        if (counts.base.failed > 0) {
            helper.logWithFormat('Failed testcases', counts.base.failed + '    <<< TEST FAILED')
        }
        helper.logWithFormat('Successful testcases', counts.base.success + ((counts.base.success > 0) && (counts.base.failed == 0) && (!getTestFailed()) ? '    <<< TEST SUCCESSFUL' : ''))
        if (counts.base.notRun > 0) {
            helper.logWithFormat('Not run testcases', counts.base.notRun)
        }
        if (counts.base.skipped > 0) {
            helper.logWithFormat('Skipped testcases', counts.base.skipped)
            if (this.skippedLevels.size() > 0) {
                if (this.level < Integer.MAX_VALUE) {
                    helper.logWithFormat('Level of run', this.level)
                }
                helper.logWithFormat('Skipped levels', this.skippedLevels)
            }
        }
        helper.logWithFormat('Total testcases', counts.base.total)

        // Restart counts reporting
        if ((counts.restarted.success > 0) || (counts.restarted.failed > 0)) {
            println ''
            helper.logWithFormat('Failed testcases after retry', counts.restarted.failed)
            helper.logWithFormat('Successful testcases after retry', counts.restarted.success)
        }

        // Setup counts reporting
        if ((counts.setUp.total > 0) && (getVerbose() || (counts.setUp.failed > 0))) {
            println ''
            if (counts.setUp.failed > 0) {
                helper.logWithFormat('Failed setUp testcases', counts.setUp.failed + '    <<< TEST FAILED')
            }
            helper.logWithFormat('Successful setUp testcases', counts.setUp.success)
            if (counts.setUp.notRun > 0) {
                helper.logWithFormat('Not run setUp testcases', counts.setUp.notRun)
            }
            if (counts.setUp.skipped > 0) {
                helper.logWithFormat('Skipped setUp testcases', counts.setUp.skipped)
            }
            helper.logWithFormat('Total setUp testcases', counts.setUp.total)
        }

        // TearDown counts reporting
        if ((counts.tearDown.total > 0) && getVerbose()) {
            println ''
            if (counts.tearDown.failed > 0) {
                helper.logWithFormat('Failed tearDown testcases', counts.tearDown.failed)
            }
            helper.logWithFormat('Successful tearDown testcases', counts.tearDown.success)
            if (counts.tearDown.notRun > 0) {
                helper.logWithFormat('Not run tearDown testcases', counts.tearDown.notRun)
            }
            if (counts.tearDown.skipped > 0) {
                helper.logWithFormat('Skipped tearDown testcases', counts.tearDown.skipped)
            }
            helper.logWithFormat('Total tearDown testcases', counts.tearDown.total)
        }

        helper.logWithFormat('Elapsed run time', "${elapsedTime} secs " + ((elapsedTime >= 60) ? "(${new Double(elapsedTime / 60).round(1)} mins)" : ''))
        println getOutputFooter() // finalize the logged output

        if (this.xmlReport) {
            generateXmlReport(elapsedTime, counts.base.failed, counts.base.skipped + counts.base.notRun, counts.base.total)
        }
        if (prepareForDbReport()) { // true if report configured and connection established
            generateDbReport(endTime, elapsedTime, counts)
        }
        if (mailReport) {
            sendMailReport(mailReport, builder, (counts.base.failed > 0) || getTestFailed())
        }
        if (imReport) {
            sendImReport(mailReport, builder, (counts.base.failed > 0) || getTestFailed())
        }
    }

    /**
     * Send mail report.
     * @param mailReport - may contain a string of , or ; separated email addresses
     * @param builder - string builder with content for mail
     * @param failed - true if test failed, false if it was successful
     */
    protected void sendMailReport(final mailReport, final builder, final boolean failed) {
        //helper.logWithFormat('mailReport', mailReport)
        if (binding.variables.containsKey('mailConfig')) {
            def config = helper.getParameterValue('mailConfig')
            def subject = '[GINT] ' + (failed ? 'FAILED' : 'SUCCESS') + " - ${getTestName()} "
            builder.insert(0, '<pre>')
            builder.append '</pre>'
            def parameters = [subject: subject, content: builder.toString(), type: 'html'] + ((mailReport instanceof String) ? [to: mailReport] : [:])
            try {
                if (!MailHelper.sendMail(config + parameters)) {
                    message 'warning', "Error trying to send mail report."
                }
            } catch(NoClassDefFoundError e) {
                message 'warning', "Mail report requested, but required jars are not on classpath. Report could not be mailed."
            }
        } else {
            message 'warning', "Mail report requested, but mailConfig property is not set. Report could not be mailed."
        }
    }

    /**
     * Send mail report.
     * @param imReport - may contain a single IM address
     * @param builder - string builder with content for mail
     * @param failed - true if test failed, false if it was successful
     */
    protected void sendImReport(final imReport, final builder, final boolean failed) {
        //helper.logWithFormat('imReport', imReport)
        if (getGintIm().getImHelper() != null) {  // error messages handled in get
            def subject = '[GINT] ' + (failed ? 'FAILED' : 'SUCCESS') + " - ${getTestName()}\n"
            def content = builder.toString()
            def parameters = [subject: subject, content: builder.toString()] + ((imReport instanceof String) ? [to: imReport] : [:])
            if (!getGintIm().getImHelper().send(parameters)) {
                message 'warning', "Error trying to send im report."
            }
        }
    }

    /**
     * Generate and write an XML Report - JUnit like (http://ant.1045680.n5.nabble.com/schema-for-junit-xml-output-td1375274.html)
     * Note that testcase timings will only be accurate if maxParallel is set to 1
     * @return
     */
    protected void generateXmlReport(final elapsedTime, final failures, final skipped, final total) {

        def xmlOutput = new FileOutputStream(new File(this.xmlReport))
        def writer = new OutputStreamWriter(xmlOutput, XML_ENCODING)
        writer.write '<?xml version="1.0"?>\n' // clear and write
        def builder = new MarkupBuilder(writer)

        builder.testsuite(time: elapsedTime, failures: "${failures}", errors: "0", skipped: "${skipped}",
        tests: "${total}", name: getTestName()) {

            try {
                def filter = getReportFilter()
                def passwordFilter = helper.getPasswordFilter() // ALWAYS apply the password filter (not optional)
                binding.ant.project.properties.each { entry ->
                    if (((filter == null) || !filter(entry.key)) && !passwordFilter(entry.key)) { // exclude some or all properties from the report file
                        builder.property(name: entry.key, value: entry.value)
                    }
                }
            } catch (Exception exception) {
                message 'warning', 'Ignoring problems adding properties to XML report.'
                if (helper.isDebug()) {
                    exception.printStackTrace()
                }
            }
            getTestcases().each { testcase ->
                //if (!testcase.isSetUp && !testcase.isTearDown) {
                if (!testcase.isTearDown) {  // include setup testcases in the XML report
                    if (testcase in this.testcasesRun) {
                        def timeDifference = ((testcase?.endTime == null) || (testcase?.startTime == null)) ? 0 : testcase.endTime - testcase.startTime
                        builder.testcase(name: testcase?.name, time: timeDifference/1000) {
                            if (!testcase.success) {
                                builder.failure(message: testcase.failReason, type: 'junit.framework.ComparisonFailure', testcase.failDetail)
                            }
                        }
                    } else { // if its not run, then need to consider it skipped from an XML report perspective
                        builder.testcase(name: testcase.name) { builder.skipped() }
                    }
                }
            }
        }
        writer << System.properties.'line.separator'
    }

    /**
     * Generate and write an XML Report - JUnit like (http://ant.1045680.n5.nabble.com/schema-for-junit-xml-output-td1375274.html)
     * Note that testcase timings will only be accurate if maxParallel is set to 1
     * @return
     */
    protected void generateDbReport(final endTime, final elapsedTime, final Map counts) {

        //message 'debug', 'dbReport: ' + dbReport
        def runTable = helper.getValueFromKeyValueString(dbReport, 'runTable')
        if (runTable == null) {
            runTable = 'gint_runs'
        }

        if (helper.isNotBlank(runTable)) {

            java.sql.Timestamp startTimestamp = new java.sql.Timestamp(startTime)

            def skippedLevelList = []
            skippedLevels.each { level ->
                skippedLevelList << level.toString()  // this takes care of boolean for example
            }

            def parameters = [
                connection: dbReportConnection,
                table: runTable,
                logExceptions: verbose,
                valueMap: [
                    test_name: testName,
                    start_time: startTime == null ? null : startTimestamp,
                    end_time: endTime == null ? null : new java.sql.Timestamp(endTime),
                    elapsed_seconds: elapsedTime, // in seconds?
                    gint_version: getGintVersion(),
                    run_repository: helper.getParameterValue('runRepository'),  // likely build key
                    run_build: helper.getParameterValue('runBuild'),  // likely build key
                    run_version: helper.getParameterValue('runVersion'), // likely the version of the code being tested if available
                    run_environment: helper.getParameterValue('runEnvironment'), // other optional environment data like server instance for example

                    skipped_levels: helper.listToSeparatedString(list: skippedLevelList, separator: ','),
                    auto_retry: getAutoRetry(),

                    // Normal
                    successful:           counts.base.success,
                    failed:               counts.base.failed,
                    not_run:              counts.base.notRun,
                    skipped:              counts.base.skipped,
                    total:                counts.base.total,

                    // Retry
                    restarted_successful: counts.tearDown.success,
                    restarted_failed:     counts.tearDown.failed,

                    // Set up
                    setup_successful:     counts.setUp.success,
                    setup_failed:         counts.setUp.failed,
                    setup_not_run:        counts.setUp.notRun,
                    setup_skipped:        counts.setUp.skipped,
                    setup_total:          counts.setUp.total,

                    // Tear down
                    teardown_successful:  counts.tearDown.success,
                    teardown_failed:      counts.tearDown.failed,
                    teardown_not_run:     counts.tearDown.notRun,
                    teardown_skipped:     counts.tearDown.skipped,
                    teardown_total:       counts.tearDown.total,

                    // environment
                    work_directory: System.properties.'user.dir',
                    ip_address: java.net.InetAddress.getLocalHost().toString(),
                    os: System.properties.'os.name',
                    os_version: System.properties.'os.version',
                    java_version: System.properties.'java.runtime.version',
                ],
            ]

            try {

                binding.sqlHelper.runInsert(parameters)

                // find the id of the row we just added above
                def runId = binding.sqlHelper.getFirstColumn(
                                                connection: dbReportConnection,
                                                table: runTable,
                                                verbose: false,
                                                sql: "select id from ${runTable} where start_time = '${startTimestamp}'",
                                                )

                message 'info', 'Run summary row added to DB report table ' + runTable + ' with id ' + runId + '.'

                def count = addDbReportLogRows(runId)

            } catch (SQLException exception) {
                addTestFailedMessage('Problem with the DB report run table. Exception: ' + exception.getMessage())
            }
        }
    }

    /**
     * Add a row for each testcase to the log table
     */
    protected int addDbReportLogRows(final runId) {

        int count = 0

        def logTable = helper.getValueFromKeyValueString(dbReport, 'logTable') // not defaulted, example name would be gint_log
        if (helper.isNotBlank(logTable)) {

            try {
                // Only log testcases that were run and started (not failed before start)
                getTestcasesRun().each { testcase ->

                    if (testcase.startTime != null) {
                        // helper.logWithFormat('testcase', testcase)
                        // helper.logWithFormat('testcase name', testcase.name)

                        def cmd = getCmd(testcase)
                        cmd = (helper.isBlank(cmd) ? null : cmd.trim())

                        def level = testcase.level
                        if (level instanceof Closure) {
                            try {
                                level = (level.getMaximumNumberOfParameters() > 0) ? level(testcase) : level()
                            } catch (Exception ignore) {
                                level = ''
                            }
                        }
                        // special case expected closure matches against result for true or false
                        def expectedAsInteger
                        try {
                            expectedAsInteger = getValueAsInteger(testcase.expected instanceof Closure ? testcase.expected(testcase.result) : testcase.expected)
                        } catch (Exception ignore) {
                            expectedAsInteger = 0
                        }
                        def parameters = [
                            connection: dbReportConnection,
                            table: logTable,
                            logExceptions: verbose,
                            valueMap: [
                                run_id: runId,  // run summary record this is associated to
                                testcase_name: testcase.name,
                                cmd: cmd,
                                // inline: testcase.inline,
                                is_set_up: getValueAsBoolean(testcase.isSetUp),
                                is_tear_down: getValueAsBoolean(testcase.isTearDown),
                                cmd_log: testcase.cmdLog != false,
                                level: level,

                                result: getValueAsInteger(testcase.result),
                                expected: expectedAsInteger,
                                ignore_failure: getValueAsBoolean(testcase.ignoreFailure),
                                success: getValueAsBoolean(testcase.success),
                                restart_count: getValueAsInteger(testcase.restartCount),

                                start_time: testcase.startTime == null ? null : new java.sql.Timestamp(testcase.startTime),
                                end_time: testcase.endTime == null ? null : new java.sql.Timestamp(testcase.endTime),
                                elapsed_seconds: ((testcase?.endTime == null) || (testcase?.startTime == null)) ? null : (testcase.endTime - testcase.startTime)/1000,
                            ],
                        ]
                        count += binding.sqlHelper.runInsert(parameters)
                    }
                }

                message 'info', count + ' rows added to DB report log table ' + logTable + '.'

            } catch (SQLException exception) {
                addTestFailedMessage('Problem with the DB report log table. Exception: ' + exception.getMessage())
                count = -1
            }
        }
        return count
    }

    protected boolean getValueAsBoolean(value) {
        if (value instanceof Closure) {
            value = value(testcase)
        }
        if (value instanceof Boolean) {
            return value
        } else if (value instanceof Integer) {
            return value == 0 ? false : true
        }
        return (value == null ? false : true)
    }

    protected int getValueAsInteger(value) {
        if (value instanceof Closure) {
            value = value(testcase.result)
        }
        if (value instanceof Integer) {
            return value
        } else if (value instanceof Boolean) {
            return (value ? 0 : -1)
        } else if (value instanceof Collection) {
            return 0
        } else if (value instanceof String || value instanceof GString) {
            return helper.getIntegerValue(value)
        }
        return 0
    }

    /**
     * Handle result message after the testcase has been run. put out a meaningful message on why a testcase failed.
     * @param testcase
     */
    protected void handleResultMessage(final Map testcase, boolean ignoreFailure = false) {
        //helper.logWithFormat('testcase', testcase)
        def cmd = getCmd(testcase)
        if (testcase.success) {
            if (getVerbose()) {
                message 'success', testcase.name + (cmd ? ': ' + cmd : '')
                //message 'success', testcase.name + (cmd ? ': ' + cmd + ((testcase.standardInput != null) ? '\n' + testcase.standardInput : '') : '')  // standard input
            }
        } else {
            def fragment = 'found in output: ' + testcase.outData
            // go in reverse order of checking as the values are not set if that condition was not checked because of earlier failure
            if (testcase.failReason == null) {  // fail reason not already set
                testcase.failReason =  // needed for XML report
                                                (testcase.closureResult == false ? "Success closure failed."
                                                : (testcase.imResult?.after == false ? "IM after response indicated failure."
                                                : (testcase.compareSuccess == false ? "File compare failed."
                                                : ((testcase.output instanceof Map) && (testcase.output.exists == false) ? "Could not find expected output file ${testcase.output.file}"
                                                : ((testcase.output instanceof Map) && (testcase.output.notFound == false) ? "Unexpected data ${helper.quoteString(helper.getValueHandleClosure(testcase.output.failData, testcase))} found in file ${testcase.output.file}"
                                                : ((testcase.output instanceof Map) && (testcase.output.found == false) ? "Expected data ${helper.quoteString(helper.getValueHandleClosure(testcase.output.data, testcase))} not found in file ${testcase.output.file}"
                                                : (testcase.notFound == false ? "Unexpected data ${helper.quoteString(helper.getValueHandleClosure(testcase.failData, testcase))} ${fragment}"
                                                : (testcase.found == false ? "Expected data ${helper.quoteString(helper.getValueHandleClosure(testcase.data, testcase))} not ${fragment}"
                                                : (testcase.imResult?.before == false ? "IM before response indicated failure."
                                                //    : (testcase.expected != testcase.result ? "Expected ${testcase.expected} but got ${testcase.result}"
                                                : (!isExpectedResult(testcase.result, testcase.expected) ?
                                                "Expected ${(testcase.expected != null) ? testcase.expected.toString() : 0 } but got ${testcase.result}"
                                                : '' ) ) ) ) ) ) ) ) ) )
            }
            def messageLength = [
                testcase.failReason.size(),
                getVerbose() ? DISPLAY_LIMIT : DISPLAY_LIMIT_SUMMARY
            ].min()
            message ignoreFailure ? 'ignore' : 'failed', "${testcase.name}: ${testcase.failReason.substring(0, messageLength)}"
            if (cmd) {
                message 'reproduce', cmd
                if (getShowFailDetail()) {
                    testcase.failDetail = cmd + ((testcase.standardInput != null) ? '\n' + helper.getValueHandleClosure(testcase.standardInput) : '')
                }
            }
            if (!testcase.success && helper.isDebug()) {
                helper.logWithFormat('testcase', testcase)
            }
        }
    }

    /**
     * Run all tearDown testcases
     */
    public void doTearDown(forceTearDown = false) {
        if (!getNoTearDown() || forceTearDown) {
            // don't tearDown on failure to help with problem determination
            // if forceTearDown, this means it is coming from tearDown target which means must be run even if run at the start
            if (!getTestFailed() && (!this.hasTearDownRun || forceTearDown)) { // don't tearDown on failure to help with problem determination
                endStartedTestcases() // make sure everything is complete before starting tearDown
                getTearDownTestcases().each { testcase ->
                    // run each tear down testcase
                    testcasesRun.remove(testcase)  // may have been run earlier, this fowls up end processing if it already has been run
                    runTestcase(testcase)
                }
                if (getVerbose() && (getTearDownTestcases().size() > 0) && (this.testcasesStarted.size() > 0)) {
                    message 'info', "Wait for completion of tearDown testcases.\n"
                }
                endStartedTestcases() // make sure tearDown finished before starting anything else
                this.hasTearDownRun = true
                saveSetUpStatus(false)
            }
        }
    }

    /**
     * Find a testcase by name in the list of all testcases
     * @param name of testcase
     * @param additionalMessage - added to failure message
     * @oaram failTestOnNotFound - true to fail the testcase if not found, false to just return null if not found
     * @result testcase if found, otherwise null.
     */
    public Map findTestcase(final name, final additionalMessage = '', final failTestOnNotFound = false) {
        def result = helper.findMapEntry(getAllTestcases(), 'name', name.toString())
        if (result == null) {
            def errorMessage = "Testcase ${name} not found. " + additionalMessage
            if (failTestOnNotFound) {
                addTestFailedMessage(errorMessage)
            }
        }
        return result
    }

    /**
     * Get the testcase output data as a string
     */
    public String getTestcaseOutDataAsString(final name, final separator = System.properties.'line.separator') {
        return helper.listToSeparatedString(findTestcase(name)?.outData, separator) // lines separated by separator
    }

    /**
     * Quick check whether we have done the setUp
     * @return true if setUp has been done, otherwise false
     */
    public boolean isSetUpDone() {
        return ((this.clean || (this.properties == null)) ? false : this.properties.getProperty('setUpDone').equals('true'))
    }

    /**
     * Save setUp status in property file - this is primarily for handling running individual testcases quickly
     */
    protected void saveSetUpStatus(final boolean done) {
        if (this.properties != null) {
            this.properties.setProperty('setUpDone', (done ? 'true' : 'false'))
            helper.storeProperties(this.propertyFile, this.properties)
        }
    }

    /**
     * Get string of failed testcases from the last run of the test
     * @result comma separated string of failed testcases
     */
    public String getFailedTestcases() {
        def failedString = this.properties ? this.properties.getProperty('failed') : null
        return (!clean && (failedString != null) && (failedString != '')) ? failedString : null
    }

    /**
     * Save list of failed testcase names
     */
    protected void saveFailedTestcases(final List failed) {
        if (this.properties != null) {
            this.properties.setProperty('failed', helper.listToSeparatedString(failed))
            helper.storeProperties(this.propertyFile, this.properties)
        }
    }

    /**
     * Add targets for each testcase in the list
     * @param list of testcases
     */
    protected void addTarget(final Collection<Map> testcaseList) {
        testcaseList.each { testcase -> addTarget(testcase) }
    }

    /**
     * Add target for an individual testcase - ensure all dependencies are handled
     * @param testcase
     */
    protected void addTarget(final Map testcase) {
        if (!helper.isNotBlank(testcase.name)) {  // null or blank name
            testcase.name = getNextName()  // automatically name the testcase
        }
        testcase.name = testcase.name.toString()  // prevents GString values from messing up Gant targets
        if (testcase.depends instanceof Boolean) {  // special case to use previous (in definition order)
            testcase.depends = (testcase.depends && this.lastTestcaseName) ? this.lastTestcaseName : null
        }
        this.lastTestcaseName = testcase.name
        addToGroup(testcase.group, testcase) // handle group

        binding.target.call(name: testcase.name, description: 'Testcase: ' + (testcase.description ?: testcase.name), prehook: [], posthook: []) {
            depends(binding.setUp)
            if (!this.hasTearDownRun) {  // may need to do individual testcase tearDown
                runTearDownTestcasesForTestcase(testcase)
            }
            def missingDepends = false
            if (!getIgnoreDepends()) {
                //message 'debug', "testcase: ${testcase.name}, depends: ${getDependsTargetNames(testcase)}"
                getDependsTargetNames(testcase).each { name ->
                    if (helper.isTargetDefined(name)) {
                        try {
                            depends(name)
                        } catch (Exception exception) {
                            addTestFailedMessage("Error with testcase ${testcase.name} depends entry ${name}.")
                            if (exception.toString().contains('depends called with an argument')) {
                                message 'error', "${name} no longer represents a testcase or target name." +
                                                                " Define ${name} or check for overlapping variable and target name."
                            } else {  // not sure how to get here, but just in case
                                message 'error', exception.toString()
                                if (helper.isVerbose()) {
                                    exception.printStackTrace()
                                }
                            }
                            missingDepends = true
                        }
                    } else {
                        addTestFailedMessage("Depends entry ${name} not found for testcase: ${testcase.name}")
                        missingDepends = true
                    }
                }
            }
            if (missingDepends) {
                checkStopConditions(testcase)
            }
            if (!getStopNow() && !missingDepends) {
                // prevent testcase being started more than once
                if (okToRun(testcase.name)) {
                    runTestcase(testcase)
                }
            }
        }
    }

    /**
     * Is this testcase ok to be started because is hasn't already been started or run
     *
     * @param name - of testcase
     * @return true if not already started or run
     */
    protected boolean okToRun(def name) {
        def findTestcase = helper.findMapEntry(getTestcasesStarted(), 'name', name)
        boolean okToRun = (findTestcase == null)
        if (okToRun) {
            findTestcase = helper.findMapEntry(getTestcasesRun(), 'name', name)
            okToRun = (findTestcase == null)
        }
        return okToRun
    }

    /**
     * Add group target - target that has all (non-setup, non-teardown) testcase in depends list
     * @param name of the group - a target gets defined for this name
     * @param list of testcases
     */
    protected void addTarget(final name, final Collection<Map> testcaseList) {
        binding.target.call(name: name.toString(), description: 'Testcase group', prehook: [], posthook: []) {
            depends('setUp')
            testcaseList.each { testcase ->
                if ((testcase.isSetup == true) || (testcase.isTearDown == true)) {
                    // ignore
                } else {
                    depends(testcase.name)
                }
            }
            // this only will do something when adding group targets during finalize
            //message 'debug', "addTarget name: ${name}, target: ${getDependsTargetNamesFromNeededBy(name)}"
            getDependsTargetNamesFromNeededBy(name).each { targetName -> depends(targetName) }
        }
    }

    /**
     * Add or update the group list with this testcase
     * @param group name or collection - testcase will be added to each
     * @param testcase to add to the group(s)
     */
    protected void addToGroup(final group, final Map testcase) {
        helper.getAsCollection(group).each { name ->
            if (groupListMap[name] == null) {
                groupListMap[name] = []as Set
            }
            groupListMap[name] << testcase
        }
    }

    /**
     * Run tearDown testcases for a testcase
     */
    protected void runTearDownTestcasesForTestcase(final Map testcase, final int depth = 1) {
        if (!isSkipped(testcase.level, testcase)) {
            getTearDownTestcaseNames(testcase).each { name ->
                def tearDownTestcase = findTestcase(name, "TearDown testcase for testcase: " + testcase.name, true)  // verify its a defined testcase
                if (tearDownTestcase) {
                    if (depth < 5) {
                        runTearDownTestcasesForTestcase(tearDownTestcase, depth + 1) // recurse and hope there is no looping
                    } else {
                        message 'warning', 'TearDown testcase looping has been stopped.'
                    }
                    if (runTestcase(tearDownTestcase, true, true)) { // ignore errors, end all started to avoid complexities
                        endTestcaseHandleRestart(tearDownTestcase, true) // make sure this is ended and not just restarted
                    }
                }
            }
        }
    }

    /**
     * Add command list target
     * - this is a non-dynamic list (does not take into account the runtime)
     * - use cmdLog for a runtime (actual) list
     */
    protected void addCommandListTarget() {
        binding.target.call(name: 'commandList', description: 'GINT: List all commands that would be run.', prehook: [], posthook: []) {
            message 'info', 'Command list'
            testcases.each { testcase ->
                def cmd = getCmd(testcase)
                if (cmd) {  // only log commands for non-inline testcases
                    println cmd  // use print so log is cleaner
                }
            }
        }
    }

    /**
     * Does the result match the expected testcase result(s)?  Support recursion for collections, strings.
     * - Handle both windows and non-windows result codes- windows uses -1, -2, etc... that gets converted to positive number on unix via 256 + number.
     * - expected null matches to 0
     * - boolean expected: true maps to 0, false to -1
     * - strings are converted to ints
     * @param expected (usually testcase.expected)
     * @param result
     * @param name - testcase name, used for diagnostic message only
     * @return true if result match based on standard behavior
     */
    protected boolean isExpectedResult(final result, final expected, final name = null) {
        try {
            return (result == expected) ||
            ((expected == null) ? (result == 0)
            : ((expected instanceof Boolean) ? ((expected && (result == 0)) || (!expected && ((result == -1) || (result == 255))))
            : ((expected instanceof Integer) ? (result == (256 + expected))
            : ((expected instanceof Collection) ? isExpectedResultWithCollection(result, expected, name)
            : ((expected instanceof String) || (expected instanceof GString)) ? isExpectedResultWithString(result, expected, name)
            : ((expected instanceof Closure) ? expected(result)
            : false
            )))))
        } catch (Exception exception) {
            if (getVerbose()) {
                message 'warning', "Exception in expected closure ignored. 0 will be used. " + exception.toString()
            }
            return (result == 0)
        }
    }

    /**
     * Expected value is a string or GString, need to convert to integer
     * @param result
     * @param expected
     * @return true if result match the converted value or matches 0 if expected is invalid
     */
    protected boolean isExpectedResultWithString(final Integer result, final expected, final name = null) {
        try {
            def expectedAsInt = Integer.parseInt(expected)
            return isExpectedResult(result, expectedAsInt)
        } catch (NumberFormatException exception) {
            if (name != null) {
                message 'warning', "Invalid expected value of '${expected}' for testcase ${name}. 0 will be used."
            }
            return (result == 0)
        }
    }

    /**
     * Expected value is a collection, need to match on ANY element of collection - support recursion
     * @param result
     * @param expected
     * @return true if result match the converted value or matches 0 if expected is invalid
     */
    protected boolean isExpectedResultWithCollection(final Integer result, final Collection list, final name = null) {
        for (expected in list) {  // use for loop for early return capability
            if (isExpectedResult(result, expected, name)) {
                return true
            }
        }
        return false
    }

    /**
     * Make sure expected value is converted to an integer
     * - boolean false to -1
     * - string value to integer it represents, -1 if not valid
     * @param expected - current expected value
     * @param name - testcase name if available for diagnostic message
     * @return converted expect value
     */
    protected int convertExpectedToInteger(final expected, final name = null) {
        def int result = 0  // default if nothing else is valid
        if (expected == null) {
            // default 0
        } else if (expected instanceof Integer) {
            result = expected
        } else if ((expected instanceof String) || (expected instanceof GString)) {
            try {
                result = Integer.parseInt(expected)
            } catch (NumberFormatException exception) {
                message 'warning', "Invalid expected value of '${expected}' for testcase ${name}. 0 will be used."
            }
        } else if (expected instanceof Boolean) {
            result = expected ? 0 : -1  // convert false to -1 (fail)
        } else {
            message 'warning', "Invalid expected value of '${expected}'" + (name ? " for testcase ${name}" : '') + ". 0 will be used."
        }
        return result
    }

    /**
     * get an input file name, add .txt if no extension is provided
     */
    public String getInputFile(final name, final whenNoExtension = '.txt') {
        return getInputDirectory() + System.properties.'file.separator' + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * get an output file name, add .txt if no extension is provided
     */
    public String getOutputFile(final name, final whenNoExtension = '.txt') {
        return getOutputDirectory() + System.properties.'file.separator' + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * get an resource file name, add .txt if no extension is provided
     */
    public String getResourceFile(final name, final whenNoExtension = '.txt') {
        return getResourceDirectory() + System.properties.'file.separator' + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * get compare file name, add .txt if no extension is provided
     */
    public String getCompareFile(final name, final whenNoExtension = '.txt') {
        return getCompareDirectory() + System.properties.'file.separator' + name + ((name?.indexOf('.') >= 0) ? '' : whenNoExtension)
    }

    /**
     * Ref: http://docs.freebsd.org/info/diff/diff.info.diff_Options.html
     * Ref: whitespace: http://docs.freebsd.org/info/diff/diff.info.White_Space.html
     * Ref: Windows diff: http://gnuwin32.sourceforge.net/packages/diffutils.htm (will need to on your path)
     * @param options
     * @return
     */
    public String getDiffOptionString(final Map options) {
        def builder = new StringBuilder()
        if (options != null) {
            if (options.ignoreCase == true) {
                builder.append('-i ')
            }
            if (options.ignoreWhiteSpaceChanges == true) {
                builder.append('-b ')
            }
            if (options.ignoreAllWhiteSpace == true) {
                builder.append('-w ')
            }
            if (options.ignoreBlankLines == true) {
                builder.append('-B ')
            }
            if (options.options != null) {  // user beware!
                builder.append(options.options).append(' ')
            }
        }
        return builder.toString()
    }

    /**
     * Run a diff command based on the compare options provided in testcase.compare
     * <p> Compare options - a Map or boolean:
     * <ul>
     * <li> true - compare using defaults
     * <li> fileName - fileName to use to generate appropriate default compare files from compare and output directories
     * <li> file1 - file path for the first file to compare, usually the expected file (defaults from compare directory)
     * <li> file2 - file path for the second file to compare, usually the newly generated file (defaults from output directory)
     * <li> fileDiff - file path for logging the output of the diff operation or true to generate a default. Null means no output is generated.
     * <li> expected - expected number of line differences, no failure if differences <= expected
     * <li> ignoreCase - ignore case differences (case-insensitive)
     * <li> ignoreWhiteSpaceChanges - ignore differences that are just the number of whites spaces
     * <li> ignoreAllWhiteSpace - ignore all whitespace, so "a a" is equivalent to "aa"
     * <li> ignoreBlankLines - ignore blank line differences
     * <li> options - string of diff options
     * </ul>
     * @param testcase
     * @return result - true is diff matched expected, false if not
     */
    public boolean runDiff(final Map testcase) {
        def compare = testcase.compare
        if (compare == true) { // convenience
            compare = [:]
        }
        //message 'debug', 'compare: ' + compare
        def result = true
        if ((compare instanceof Map) && helper.isNotBlank(getDiffCli())) {
            def defaultFileName = compare.fileName ?: testcase.name  // simple file name for default case
            def extension = (compare.asDir == true) ? '' : '.txt' // option to compare entire directory
            def file1 = (compare.file1 ?: getCompareFile(defaultFileName, extension))
            def file2 = (compare.file2 ?: getOutputFile(defaultFileName, extension))
            // Make sure we have something meaningful to compare. This allows for compares only when compare label is provided.
            if (file1 != file2) {
                def cmd = "${getDiffCli()} ${getDiffOptionString(compare)} \"${file1}\" \"${file2}\""
                if (getVerbose()) {
                    message 'info', "${cmd}"
                }
                def expected = convertExpectedToInteger(compare.expected, testcase.name)
                //(compare.expected instanceof Integer) ? compare.expected : 0
                def p = parallel.shell(cmd)
                def rc = p.end(false)  // don't log output, handle later here
                result = (rc == 0) || (rc == 1)  // rc == 1 indicates file differences - check those below
                if (!result) {
                    message 'error', "File compare failed with return code: ${rc}"
                    p.printErrorList()
                } else {
                    def differenceCount = 0  // count actual line differences
                    def differenceList = p.getOutList()
                    def ignoreString = getDiffCli().substring(0, 6).toLowerCase() // line starting with this should be ignored
                    differenceList.each { line ->
                        if (!line.startsWith(ignoreString)) {  // NOT a line specifying the file being compared for dir compares
                            differenceCount++
                        }
                    }
                    if (differenceCount > 0) {
                        message(result ? 'info' : 'error', differenceCount + " differences found comparing ${file1} to ${file2}. No more than ${expected} were expected.")
                        if (compare.fileDiff != null) {
                            def fileName = (compare.fileDiff == true) ? getOutputFile(testcase.name, '.diff') : compare.fileDiff
                            new File(fileName).withWriter { out ->
                                differenceList.each { line -> out.writeLine(line) }
                            }
                            message 'info', differenceCount + " differences written to ${fileName}"
                        } else if (getVerbose()) {
                            p.printOutList() // log it here
                            p.printErrorList() // log it here
                        }
                    }
                    result = (differenceCount <= expected)
                }
            }
        }
        return result
    }

    /**
     * Run a image command based on the imageCompare options provided in testcase.imageCompare
     * <p> Compare options - a Map or boolean:
     * <ul>
     * <li> true - compare using defaults
     * <li> fileName - fileName to use to generate appropriate default compare files from compare and output directories
     * <li> file1 - file path for the first file to compare, usually the expected file (defaults from compare directory)
     * <li> file2 - file path for the second file to compare, usually the newly generated file (defaults from output directory)
     * <li> fileDiff - file path for diff image. Null means no output is generated.
     * <li> expected - expected number of region differences, no failure if region differences <= expected
     * <li> options - string of image compare parameters
     * </ul>
     * @param testcase
     * @return result - true is compare matched expected, false if not
     */
    public boolean runImageCompare(final Map testcase) {
        def imageCli = helper.getParameterValue('imageCli', null)
        def compare = testcase.imageCompare
        if (compare == true) { // convenience
            compare = [:]
        }
        if (helper.isDebug()) {
            message 'debug', 'imageCli: ' + imageCli
            message 'debug', 'compare: ' + compare
        }
        def result = true
        if ((compare instanceof Map) && helper.isNotBlank(imageCli)) {
            def defaultFileName = compare.fileName ?: testcase.name + '.png' // simple file name for default case
            def extension = (compare.asDir == true) ? '' : '.txt'  // option to compare entire directory
            def file1 = (compare.file1 ?: getCompareFile(defaultFileName, extension))  // expected file
            def file2 = (compare.file2 ?: getOutputFile(defaultFileName, extension))   // new file, usually generated

            // Make sure we have something meaningful to compare. This allows for compares only when compare label is provided.
            if (getVerbose()) {
                message 'info', "image compare file1: ${file1}"
                message 'info', "image compare file2: ${file2}"
            }
            if ((file1 != file2) && helper.isNotBlank(file1) && helper.isNotBlank(file2)) {
                def pFile = ''
                def fileName = (compare.fileDiff == true) ? getOutputFile(testcase.name, '_compare.png') : compare.fileDiff
                if (fileName != null) {
                    pFile = "--file ${fileName}"
                }
                def options = compare.options ?: ''
                def expected = convertExpectedToInteger(compare.expected, testcase.name)
                //(compare.expected instanceof Integer) ? compare.expected : 0
                def cmd = imageCli + " --action compare --inputFile ${file1} --inputFile2 ${file2} ${pFile} ${options} --tolerate ${expected}"
                if (getVerbose()) {
                    message 'info', "${cmd}"
                }
                def p = parallel.shell(cmd)
                def rc = p.end(false)  // don't log output, handle later here
                result = (rc == 0)
                if (!result) {
                    message 'error', "Image compare failed with return code: ${rc}"
                    p.printErrorList()
                } else if (getVerbose()) {
                    p.printOutList() // log it here
                }
            }
        }
        return result
    }
}